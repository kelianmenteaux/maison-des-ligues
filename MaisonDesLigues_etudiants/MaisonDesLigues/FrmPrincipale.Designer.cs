﻿namespace MaisonDesLigues
{
    partial class FrmPrincipale
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPrincipale));
            this.TabInscription = new System.Windows.Forms.TabPage();
            this.GrpLicencie = new System.Windows.Forms.GroupBox();
            this.GrbAccompagant = new System.Windows.Forms.GroupBox();
            this.ChkDimancheMidi = new System.Windows.Forms.CheckBox();
            this.ChkSamediSoir = new System.Windows.Forms.CheckBox();
            this.ChkSamediMidi = new System.Windows.Forms.CheckBox();
            this.label23 = new System.Windows.Forms.Label();
            this.maskedTextBox2 = new System.Windows.Forms.MaskedTextBox();
            this.NudValChequeAcc = new System.Windows.Forms.NumericUpDown();
            this.label28 = new System.Windows.Forms.Label();
            this.LbxAtelierLicencie = new System.Windows.Forms.ListBox();
            this.GrpPaiemantLicencie = new System.Windows.Forms.GroupBox();
            this.label24 = new System.Windows.Forms.Label();
            this.TxtNumChequeLicencieTotal = new System.Windows.Forms.MaskedTextBox();
            this.NudValeurChequeLicencieTout = new System.Windows.Forms.NumericUpDown();
            this.label25 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.CbxQualiteLicencie = new System.Windows.Forms.ComboBox();
            this.RdbSeparement = new System.Windows.Forms.RadioButton();
            this.GrpNuiteLicencie = new System.Windows.Forms.GroupBox();
            this.PanNuiteLicencie = new System.Windows.Forms.Panel();
            this.RdbNuiteLicencieNon = new System.Windows.Forms.RadioButton();
            this.RdbNuiteLicencieOui = new System.Windows.Forms.RadioButton();
            this.BtnEnregistrerLicencie = new System.Windows.Forms.Button();
            this.TxtNumLicenceLicencie = new System.Windows.Forms.MaskedTextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.RdbTout = new System.Windows.Forms.RadioButton();
            this.GrpBenevole = new System.Windows.Forms.GroupBox();
            this.BtnEnregistreBenevole = new System.Windows.Forms.Button();
            this.PanelDispoBenevole = new System.Windows.Forms.Panel();
            this.TxtLicenceBenevole = new System.Windows.Forms.MaskedTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtDateNaissance = new System.Windows.Forms.MaskedTextBox();
            this.GrpTypeParticipant = new System.Windows.Forms.GroupBox();
            this.RadLicencie = new System.Windows.Forms.RadioButton();
            this.RadBenevole = new System.Windows.Forms.RadioButton();
            this.RadIntervenant = new System.Windows.Forms.RadioButton();
            this.GrpIdentite = new System.Windows.Forms.GroupBox();
            this.TxtMail = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTel = new System.Windows.Forms.MaskedTextBox();
            this.TxtCp = new System.Windows.Forms.MaskedTextBox();
            this.TxtVille = new System.Windows.Forms.TextBox();
            this.TxtAdr2 = new System.Windows.Forms.TextBox();
            this.TxtAdr1 = new System.Windows.Forms.TextBox();
            this.TxtPrenom = new System.Windows.Forms.TextBox();
            this.TxtNom = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.GrpIntervenant = new System.Windows.Forms.GroupBox();
            this.BtnEnregistrerIntervenant = new System.Windows.Forms.Button();
            this.GrpNuiteIntervenant = new System.Windows.Forms.GroupBox();
            this.PanNuiteIntervenant = new System.Windows.Forms.Panel();
            this.RdbNuiteIntervenantNon = new System.Windows.Forms.RadioButton();
            this.RdbNuiteIntervenantOui = new System.Windows.Forms.RadioButton();
            this.PanFonctionIntervenant = new System.Windows.Forms.Panel();
            this.CmbAtelierIntervenant = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.CmdQuitter = new System.Windows.Forms.Button();
            this.PicAffiche = new System.Windows.Forms.PictureBox();
            this.TabPrincipal = new System.Windows.Forms.TabControl();
            this.TabAjout = new System.Windows.Forms.TabPage();
            this.GbMajVacation = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.DpDateDebutMaj = new System.Windows.Forms.DateTimePicker();
            this.DpHeureFinMaj = new System.Windows.Forms.DateTimePicker();
            this.DpHeureDebutMaj = new System.Windows.Forms.DateTimePicker();
            this.DpDateFinMaj = new System.Windows.Forms.DateTimePicker();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.CbxModificationVac = new System.Windows.Forms.ComboBox();
            this.CbxModifVacation = new System.Windows.Forms.ComboBox();
            this.BtnMaj = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.GbAjout = new System.Windows.Forms.GroupBox();
            this.RdTheme = new System.Windows.Forms.RadioButton();
            this.RdAtelier = new System.Windows.Forms.RadioButton();
            this.RdVacation = new System.Windows.Forms.RadioButton();
            this.GbVacation = new System.Windows.Forms.GroupBox();
            this.DpHeureFin = new System.Windows.Forms.DateTimePicker();
            this.DpDateFin = new System.Windows.Forms.DateTimePicker();
            this.DpHeureDebut = new System.Windows.Forms.DateTimePicker();
            this.DpDateDebut = new System.Windows.Forms.DateTimePicker();
            this.CbAtelierVacation = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.BtnAjoutVacation = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.GbTheme = new System.Windows.Forms.GroupBox();
            this.CmbAtelierAjoutTheme = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.BtnAjoutTheme = new System.Windows.Forms.Button();
            this.txtBoxNomTheme = new System.Windows.Forms.TextBox();
            this.labelNom = new System.Windows.Forms.Label();
            this.GbAtelier = new System.Windows.Forms.GroupBox();
            this.BtnAjoutAtelier = new System.Windows.Forms.Button();
            this.labelNbPlaceMax = new System.Windows.Forms.Label();
            this.numUpDownNbPlace = new System.Windows.Forms.NumericUpDown();
            this.txtBoxNomAtelier = new System.Windows.Forms.TextBox();
            this.labelNomAtelier = new System.Windows.Forms.Label();
            this.TabArrive = new System.Windows.Forms.TabPage();
            this.GrpHeureArriver = new System.Windows.Forms.GroupBox();
            this.BtnValiderHeureArriver = new System.Windows.Forms.Button();
            this.DtpDateArriver = new System.Windows.Forms.DateTimePicker();
            this.CmbParticipant = new System.Windows.Forms.ComboBox();
            this.DtpHeureArriver = new System.Windows.Forms.DateTimePicker();
            this.TabInscription.SuspendLayout();
            this.GrpLicencie.SuspendLayout();
            this.GrbAccompagant.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NudValChequeAcc)).BeginInit();
            this.GrpPaiemantLicencie.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NudValeurChequeLicencieTout)).BeginInit();
            this.GrpNuiteLicencie.SuspendLayout();
            this.GrpBenevole.SuspendLayout();
            this.GrpTypeParticipant.SuspendLayout();
            this.GrpIdentite.SuspendLayout();
            this.GrpIntervenant.SuspendLayout();
            this.GrpNuiteIntervenant.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicAffiche)).BeginInit();
            this.TabPrincipal.SuspendLayout();
            this.TabAjout.SuspendLayout();
            this.GbMajVacation.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.GbAjout.SuspendLayout();
            this.GbVacation.SuspendLayout();
            this.GbTheme.SuspendLayout();
            this.GbAtelier.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDownNbPlace)).BeginInit();
            this.TabArrive.SuspendLayout();
            this.GrpHeureArriver.SuspendLayout();
            this.SuspendLayout();
            // 
            // TabInscription
            // 
            this.TabInscription.Controls.Add(this.GrpLicencie);
            this.TabInscription.Controls.Add(this.GrpBenevole);
            this.TabInscription.Controls.Add(this.GrpTypeParticipant);
            this.TabInscription.Controls.Add(this.GrpIdentite);
            this.TabInscription.Controls.Add(this.GrpIntervenant);
            this.TabInscription.Location = new System.Drawing.Point(4, 22);
            this.TabInscription.Name = "TabInscription";
            this.TabInscription.Padding = new System.Windows.Forms.Padding(3);
            this.TabInscription.Size = new System.Drawing.Size(794, 607);
            this.TabInscription.TabIndex = 0;
            this.TabInscription.Text = "Inscription";
            this.TabInscription.UseVisualStyleBackColor = true;
            // 
            // GrpLicencie
            // 
            this.GrpLicencie.Controls.Add(this.GrbAccompagant);
            this.GrpLicencie.Controls.Add(this.LbxAtelierLicencie);
            this.GrpLicencie.Controls.Add(this.GrpPaiemantLicencie);
            this.GrpLicencie.Controls.Add(this.label20);
            this.GrpLicencie.Controls.Add(this.CbxQualiteLicencie);
            this.GrpLicencie.Controls.Add(this.RdbSeparement);
            this.GrpLicencie.Controls.Add(this.GrpNuiteLicencie);
            this.GrpLicencie.Controls.Add(this.BtnEnregistrerLicencie);
            this.GrpLicencie.Controls.Add(this.TxtNumLicenceLicencie);
            this.GrpLicencie.Controls.Add(this.label19);
            this.GrpLicencie.Controls.Add(this.RdbTout);
            this.GrpLicencie.Location = new System.Drawing.Point(23, 274);
            this.GrpLicencie.Name = "GrpLicencie";
            this.GrpLicencie.Size = new System.Drawing.Size(765, 308);
            this.GrpLicencie.TabIndex = 26;
            this.GrpLicencie.TabStop = false;
            this.GrpLicencie.Text = "Complément Inscription Licencié";
            this.GrpLicencie.Visible = false;
            // 
            // GrbAccompagant
            // 
            this.GrbAccompagant.Controls.Add(this.ChkDimancheMidi);
            this.GrbAccompagant.Controls.Add(this.ChkSamediSoir);
            this.GrbAccompagant.Controls.Add(this.ChkSamediMidi);
            this.GrbAccompagant.Controls.Add(this.label23);
            this.GrbAccompagant.Controls.Add(this.maskedTextBox2);
            this.GrbAccompagant.Controls.Add(this.NudValChequeAcc);
            this.GrbAccompagant.Controls.Add(this.label28);
            this.GrbAccompagant.Enabled = false;
            this.GrbAccompagant.Location = new System.Drawing.Point(393, 163);
            this.GrbAccompagant.Name = "GrbAccompagant";
            this.GrbAccompagant.Size = new System.Drawing.Size(361, 97);
            this.GrbAccompagant.TabIndex = 42;
            this.GrbAccompagant.TabStop = false;
            this.GrbAccompagant.Text = "Accompagnant";
            // 
            // ChkDimancheMidi
            // 
            this.ChkDimancheMidi.AutoSize = true;
            this.ChkDimancheMidi.Location = new System.Drawing.Point(168, 23);
            this.ChkDimancheMidi.Name = "ChkDimancheMidi";
            this.ChkDimancheMidi.Size = new System.Drawing.Size(116, 17);
            this.ChkDimancheMidi.TabIndex = 43;
            this.ChkDimancheMidi.Text = "dimanche dejeuner";
            this.ChkDimancheMidi.UseVisualStyleBackColor = true;
            this.ChkDimancheMidi.CheckedChanged += new System.EventHandler(this.ChkAccompagantRepas_CheckedChanged);
            // 
            // ChkSamediSoir
            // 
            this.ChkSamediSoir.AutoSize = true;
            this.ChkSamediSoir.Location = new System.Drawing.Point(12, 46);
            this.ChkSamediSoir.Name = "ChkSamediSoir";
            this.ChkSamediSoir.Size = new System.Drawing.Size(85, 17);
            this.ChkSamediSoir.TabIndex = 42;
            this.ChkSamediSoir.Text = "samedi diner";
            this.ChkSamediSoir.UseVisualStyleBackColor = true;
            this.ChkSamediSoir.CheckedChanged += new System.EventHandler(this.ChkAccompagantRepas_CheckedChanged);
            // 
            // ChkSamediMidi
            // 
            this.ChkSamediMidi.AutoSize = true;
            this.ChkSamediMidi.Location = new System.Drawing.Point(12, 23);
            this.ChkSamediMidi.Name = "ChkSamediMidi";
            this.ChkSamediMidi.Size = new System.Drawing.Size(103, 17);
            this.ChkSamediMidi.TabIndex = 41;
            this.ChkSamediMidi.Text = "samedi dejeuner";
            this.ChkSamediMidi.UseVisualStyleBackColor = true;
            this.ChkSamediMidi.CheckedChanged += new System.EventHandler(this.ChkAccompagantRepas_CheckedChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(210, 75);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(40, 13);
            this.label23.TabIndex = 40;
            this.label23.Text = "Valeur:";
            // 
            // maskedTextBox2
            // 
            this.maskedTextBox2.Enabled = false;
            this.maskedTextBox2.Location = new System.Drawing.Point(120, 70);
            this.maskedTextBox2.Mask = "0000000";
            this.maskedTextBox2.Name = "maskedTextBox2";
            this.maskedTextBox2.Size = new System.Drawing.Size(76, 20);
            this.maskedTextBox2.TabIndex = 38;
            // 
            // NudValChequeAcc
            // 
            this.NudValChequeAcc.Location = new System.Drawing.Point(256, 70);
            this.NudValChequeAcc.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.NudValChequeAcc.Name = "NudValChequeAcc";
            this.NudValChequeAcc.Size = new System.Drawing.Size(61, 20);
            this.NudValChequeAcc.TabIndex = 39;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(9, 77);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(104, 13);
            this.label28.TabIndex = 37;
            this.label28.Text = "Numéro de chéque: ";
            // 
            // LbxAtelierLicencie
            // 
            this.LbxAtelierLicencie.FormattingEnabled = true;
            this.LbxAtelierLicencie.Location = new System.Drawing.Point(13, 74);
            this.LbxAtelierLicencie.Name = "LbxAtelierLicencie";
            this.LbxAtelierLicencie.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.LbxAtelierLicencie.Size = new System.Drawing.Size(237, 82);
            this.LbxAtelierLicencie.TabIndex = 38;
            this.LbxAtelierLicencie.SelectedIndexChanged += new System.EventHandler(this.LbxAtelierLicencie_SelectedIndexChanged);
            // 
            // GrpPaiemantLicencie
            // 
            this.GrpPaiemantLicencie.Controls.Add(this.label24);
            this.GrpPaiemantLicencie.Controls.Add(this.TxtNumChequeLicencieTotal);
            this.GrpPaiemantLicencie.Controls.Add(this.NudValeurChequeLicencieTout);
            this.GrpPaiemantLicencie.Controls.Add(this.label25);
            this.GrpPaiemantLicencie.Location = new System.Drawing.Point(13, 201);
            this.GrpPaiemantLicencie.Name = "GrpPaiemantLicencie";
            this.GrpPaiemantLicencie.Size = new System.Drawing.Size(360, 59);
            this.GrpPaiemantLicencie.TabIndex = 37;
            this.GrpPaiemantLicencie.TabStop = false;
            this.GrpPaiemantLicencie.Text = "Paiement inscription et hotel";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(208, 36);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(40, 13);
            this.label24.TabIndex = 47;
            this.label24.Text = "Valeur:";
            // 
            // TxtNumChequeLicencieTotal
            // 
            this.TxtNumChequeLicencieTotal.Location = new System.Drawing.Point(118, 30);
            this.TxtNumChequeLicencieTotal.Mask = "0000000";
            this.TxtNumChequeLicencieTotal.Name = "TxtNumChequeLicencieTotal";
            this.TxtNumChequeLicencieTotal.Size = new System.Drawing.Size(76, 20);
            this.TxtNumChequeLicencieTotal.TabIndex = 45;
            // 
            // NudValeurChequeLicencieTout
            // 
            this.NudValeurChequeLicencieTout.Location = new System.Drawing.Point(254, 30);
            this.NudValeurChequeLicencieTout.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.NudValeurChequeLicencieTout.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.NudValeurChequeLicencieTout.Name = "NudValeurChequeLicencieTout";
            this.NudValeurChequeLicencieTout.Size = new System.Drawing.Size(61, 20);
            this.NudValeurChequeLicencieTout.TabIndex = 46;
            this.NudValeurChequeLicencieTout.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(7, 37);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(104, 13);
            this.label25.TabIndex = 44;
            this.label25.Text = "Numéro de chéque: ";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(63, 53);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(49, 13);
            this.label20.TabIndex = 34;
            this.label20.Text = "Qualité : ";
            // 
            // CbxQualiteLicencie
            // 
            this.CbxQualiteLicencie.FormattingEnabled = true;
            this.CbxQualiteLicencie.Location = new System.Drawing.Point(118, 45);
            this.CbxQualiteLicencie.Name = "CbxQualiteLicencie";
            this.CbxQualiteLicencie.Size = new System.Drawing.Size(132, 21);
            this.CbxQualiteLicencie.TabIndex = 33;
            this.CbxQualiteLicencie.SelectedIndexChanged += new System.EventHandler(this.CbxQualiteLicencie_SelectedIndexChanged);
            // 
            // RdbSeparement
            // 
            this.RdbSeparement.AutoSize = true;
            this.RdbSeparement.Location = new System.Drawing.Point(23, 173);
            this.RdbSeparement.Name = "RdbSeparement";
            this.RdbSeparement.Size = new System.Drawing.Size(185, 17);
            this.RdbSeparement.TabIndex = 41;
            this.RdbSeparement.Text = "inscription + hotel + Accompagant";
            this.RdbSeparement.UseVisualStyleBackColor = true;
            this.RdbSeparement.CheckedChanged += new System.EventHandler(this.RdbSeparement_CheckedChanged);
            // 
            // GrpNuiteLicencie
            // 
            this.GrpNuiteLicencie.Controls.Add(this.PanNuiteLicencie);
            this.GrpNuiteLicencie.Controls.Add(this.RdbNuiteLicencieNon);
            this.GrpNuiteLicencie.Controls.Add(this.RdbNuiteLicencieOui);
            this.GrpNuiteLicencie.Location = new System.Drawing.Point(266, 16);
            this.GrpNuiteLicencie.Name = "GrpNuiteLicencie";
            this.GrpNuiteLicencie.Size = new System.Drawing.Size(488, 141);
            this.GrpNuiteLicencie.TabIndex = 30;
            this.GrpNuiteLicencie.TabStop = false;
            this.GrpNuiteLicencie.Text = "Nuités";
            // 
            // PanNuiteLicencie
            // 
            this.PanNuiteLicencie.Location = new System.Drawing.Point(3, 43);
            this.PanNuiteLicencie.Name = "PanNuiteLicencie";
            this.PanNuiteLicencie.Size = new System.Drawing.Size(485, 89);
            this.PanNuiteLicencie.TabIndex = 24;
            this.PanNuiteLicencie.Visible = false;
            // 
            // RdbNuiteLicencieNon
            // 
            this.RdbNuiteLicencieNon.AutoSize = true;
            this.RdbNuiteLicencieNon.Checked = true;
            this.RdbNuiteLicencieNon.Location = new System.Drawing.Point(92, 15);
            this.RdbNuiteLicencieNon.Name = "RdbNuiteLicencieNon";
            this.RdbNuiteLicencieNon.Size = new System.Drawing.Size(45, 17);
            this.RdbNuiteLicencieNon.TabIndex = 23;
            this.RdbNuiteLicencieNon.TabStop = true;
            this.RdbNuiteLicencieNon.Text = "Non";
            this.RdbNuiteLicencieNon.UseVisualStyleBackColor = true;
            this.RdbNuiteLicencieNon.CheckedChanged += new System.EventHandler(this.RdbNuiteLicencie_CheckedChanged);
            // 
            // RdbNuiteLicencieOui
            // 
            this.RdbNuiteLicencieOui.AutoSize = true;
            this.RdbNuiteLicencieOui.Location = new System.Drawing.Point(23, 16);
            this.RdbNuiteLicencieOui.Name = "RdbNuiteLicencieOui";
            this.RdbNuiteLicencieOui.Size = new System.Drawing.Size(41, 17);
            this.RdbNuiteLicencieOui.TabIndex = 22;
            this.RdbNuiteLicencieOui.Text = "Oui";
            this.RdbNuiteLicencieOui.UseVisualStyleBackColor = true;
            this.RdbNuiteLicencieOui.CheckedChanged += new System.EventHandler(this.RdbNuiteLicencie_CheckedChanged);
            // 
            // BtnEnregistrerLicencie
            // 
            this.BtnEnregistrerLicencie.Enabled = false;
            this.BtnEnregistrerLicencie.Location = new System.Drawing.Point(621, 275);
            this.BtnEnregistrerLicencie.Name = "BtnEnregistrerLicencie";
            this.BtnEnregistrerLicencie.Size = new System.Drawing.Size(133, 27);
            this.BtnEnregistrerLicencie.TabIndex = 1;
            this.BtnEnregistrerLicencie.Text = "Enregistrer";
            this.BtnEnregistrerLicencie.UseVisualStyleBackColor = true;
            this.BtnEnregistrerLicencie.Click += new System.EventHandler(this.BtnEnregistrerLicencie_Click);
            // 
            // TxtNumLicenceLicencie
            // 
            this.TxtNumLicenceLicencie.Location = new System.Drawing.Point(147, 19);
            this.TxtNumLicenceLicencie.Mask = "000000000000";
            this.TxtNumLicenceLicencie.Name = "TxtNumLicenceLicencie";
            this.TxtNumLicenceLicencie.Size = new System.Drawing.Size(103, 20);
            this.TxtNumLicenceLicencie.TabIndex = 19;
            this.TxtNumLicenceLicencie.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.TxtNumLicenceLicencie_MaskInputRejected);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(36, 26);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(105, 13);
            this.label19.TabIndex = 17;
            this.label19.Text = "Numéro de licence : ";
            // 
            // RdbTout
            // 
            this.RdbTout.AutoSize = true;
            this.RdbTout.Checked = true;
            this.RdbTout.Location = new System.Drawing.Point(224, 173);
            this.RdbTout.Name = "RdbTout";
            this.RdbTout.Size = new System.Drawing.Size(107, 17);
            this.RdbTout.TabIndex = 23;
            this.RdbTout.TabStop = true;
            this.RdbTout.Text = "inscription + hotel";
            this.RdbTout.UseVisualStyleBackColor = true;
            // 
            // GrpBenevole
            // 
            this.GrpBenevole.Controls.Add(this.BtnEnregistreBenevole);
            this.GrpBenevole.Controls.Add(this.PanelDispoBenevole);
            this.GrpBenevole.Controls.Add(this.TxtLicenceBenevole);
            this.GrpBenevole.Controls.Add(this.label9);
            this.GrpBenevole.Controls.Add(this.label8);
            this.GrpBenevole.Controls.Add(this.TxtDateNaissance);
            this.GrpBenevole.Location = new System.Drawing.Point(725, 416);
            this.GrpBenevole.Name = "GrpBenevole";
            this.GrpBenevole.Size = new System.Drawing.Size(765, 172);
            this.GrpBenevole.TabIndex = 23;
            this.GrpBenevole.TabStop = false;
            this.GrpBenevole.Text = "Disponibilités Bénévole";
            this.GrpBenevole.Visible = false;
            // 
            // BtnEnregistreBenevole
            // 
            this.BtnEnregistreBenevole.Enabled = false;
            this.BtnEnregistreBenevole.Location = new System.Drawing.Point(626, 141);
            this.BtnEnregistreBenevole.Name = "BtnEnregistreBenevole";
            this.BtnEnregistreBenevole.Size = new System.Drawing.Size(133, 25);
            this.BtnEnregistreBenevole.TabIndex = 1;
            this.BtnEnregistreBenevole.Text = "Enregistrer";
            this.BtnEnregistreBenevole.UseVisualStyleBackColor = true;
            this.BtnEnregistreBenevole.Click += new System.EventHandler(this.BtnEnregistreBenevole_Click);
            // 
            // PanelDispoBenevole
            // 
            this.PanelDispoBenevole.Location = new System.Drawing.Point(28, 84);
            this.PanelDispoBenevole.Name = "PanelDispoBenevole";
            this.PanelDispoBenevole.Size = new System.Drawing.Size(251, 84);
            this.PanelDispoBenevole.TabIndex = 21;
            // 
            // TxtLicenceBenevole
            // 
            this.TxtLicenceBenevole.Location = new System.Drawing.Point(136, 58);
            this.TxtLicenceBenevole.Mask = "000000000000";
            this.TxtLicenceBenevole.Name = "TxtLicenceBenevole";
            this.TxtLicenceBenevole.Size = new System.Drawing.Size(147, 20);
            this.TxtLicenceBenevole.TabIndex = 19;
            this.TxtLicenceBenevole.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ChkDateBenevole_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(25, 35);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(105, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "Date de naissance : ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(25, 65);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(105, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Numéro de licence : ";
            // 
            // TxtDateNaissance
            // 
            this.TxtDateNaissance.Location = new System.Drawing.Point(136, 32);
            this.TxtDateNaissance.Mask = "00/00/0000";
            this.TxtDateNaissance.Name = "TxtDateNaissance";
            this.TxtDateNaissance.Size = new System.Drawing.Size(147, 20);
            this.TxtDateNaissance.TabIndex = 2;
            this.TxtDateNaissance.Text = "15081985";
            this.TxtDateNaissance.ValidatingType = typeof(System.DateTime);
            this.TxtDateNaissance.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ChkDateBenevole_CheckedChanged);
            // 
            // GrpTypeParticipant
            // 
            this.GrpTypeParticipant.Controls.Add(this.RadLicencie);
            this.GrpTypeParticipant.Controls.Add(this.RadBenevole);
            this.GrpTypeParticipant.Controls.Add(this.RadIntervenant);
            this.GrpTypeParticipant.Location = new System.Drawing.Point(23, 6);
            this.GrpTypeParticipant.Name = "GrpTypeParticipant";
            this.GrpTypeParticipant.Size = new System.Drawing.Size(765, 58);
            this.GrpTypeParticipant.TabIndex = 21;
            this.GrpTypeParticipant.TabStop = false;
            this.GrpTypeParticipant.Text = "Type Participant";
            // 
            // RadLicencie
            // 
            this.RadLicencie.AutoSize = true;
            this.RadLicencie.Location = new System.Drawing.Point(334, 24);
            this.RadLicencie.Name = "RadLicencie";
            this.RadLicencie.Size = new System.Drawing.Size(65, 17);
            this.RadLicencie.TabIndex = 20;
            this.RadLicencie.TabStop = true;
            this.RadLicencie.Text = "Licencié";
            this.RadLicencie.UseVisualStyleBackColor = true;
            this.RadLicencie.CheckedChanged += new System.EventHandler(this.RadTypeParticipant_Changed);
            // 
            // RadBenevole
            // 
            this.RadBenevole.AutoSize = true;
            this.RadBenevole.Location = new System.Drawing.Point(592, 24);
            this.RadBenevole.Name = "RadBenevole";
            this.RadBenevole.Size = new System.Drawing.Size(70, 17);
            this.RadBenevole.TabIndex = 19;
            this.RadBenevole.TabStop = true;
            this.RadBenevole.Text = "Bénévole";
            this.RadBenevole.UseVisualStyleBackColor = true;
            this.RadBenevole.CheckedChanged += new System.EventHandler(this.RadTypeParticipant_Changed);
            // 
            // RadIntervenant
            // 
            this.RadIntervenant.AutoSize = true;
            this.RadIntervenant.Location = new System.Drawing.Point(86, 24);
            this.RadIntervenant.Name = "RadIntervenant";
            this.RadIntervenant.Size = new System.Drawing.Size(79, 17);
            this.RadIntervenant.TabIndex = 18;
            this.RadIntervenant.TabStop = true;
            this.RadIntervenant.Text = "Intervenant";
            this.RadIntervenant.UseVisualStyleBackColor = true;
            this.RadIntervenant.CheckedChanged += new System.EventHandler(this.RadTypeParticipant_Changed);
            // 
            // GrpIdentite
            // 
            this.GrpIdentite.Controls.Add(this.TxtMail);
            this.GrpIdentite.Controls.Add(this.label7);
            this.GrpIdentite.Controls.Add(this.label6);
            this.GrpIdentite.Controls.Add(this.txtTel);
            this.GrpIdentite.Controls.Add(this.TxtCp);
            this.GrpIdentite.Controls.Add(this.TxtVille);
            this.GrpIdentite.Controls.Add(this.TxtAdr2);
            this.GrpIdentite.Controls.Add(this.TxtAdr1);
            this.GrpIdentite.Controls.Add(this.TxtPrenom);
            this.GrpIdentite.Controls.Add(this.TxtNom);
            this.GrpIdentite.Controls.Add(this.label5);
            this.GrpIdentite.Controls.Add(this.label4);
            this.GrpIdentite.Controls.Add(this.label3);
            this.GrpIdentite.Controls.Add(this.label2);
            this.GrpIdentite.Controls.Add(this.label1);
            this.GrpIdentite.Location = new System.Drawing.Point(23, 70);
            this.GrpIdentite.Name = "GrpIdentite";
            this.GrpIdentite.Size = new System.Drawing.Size(765, 188);
            this.GrpIdentite.TabIndex = 17;
            this.GrpIdentite.TabStop = false;
            this.GrpIdentite.Text = "Identité";
            // 
            // TxtMail
            // 
            this.TxtMail.Location = new System.Drawing.Point(232, 148);
            this.TxtMail.Name = "TxtMail";
            this.TxtMail.Size = new System.Drawing.Size(189, 20);
            this.TxtMail.TabIndex = 16;
            this.TxtMail.Text = "patrick.dumoulin@toto.fr";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(191, 151);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Mail :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(35, 155);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(28, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Tél :";
            // 
            // txtTel
            // 
            this.txtTel.Location = new System.Drawing.Point(81, 148);
            this.txtTel.Mask = "00 00 00 00 00";
            this.txtTel.Name = "txtTel";
            this.txtTel.Size = new System.Drawing.Size(83, 20);
            this.txtTel.TabIndex = 13;
            // 
            // TxtCp
            // 
            this.TxtCp.Location = new System.Drawing.Point(81, 107);
            this.TxtCp.Mask = "00000";
            this.TxtCp.Name = "TxtCp";
            this.TxtCp.Size = new System.Drawing.Size(83, 20);
            this.TxtCp.TabIndex = 12;
            this.TxtCp.Text = "75001";
            // 
            // TxtVille
            // 
            this.TxtVille.Location = new System.Drawing.Point(232, 104);
            this.TxtVille.Name = "TxtVille";
            this.TxtVille.Size = new System.Drawing.Size(189, 20);
            this.TxtVille.TabIndex = 11;
            this.TxtVille.Text = "Paris";
            // 
            // TxtAdr2
            // 
            this.TxtAdr2.Location = new System.Drawing.Point(81, 76);
            this.TxtAdr2.Name = "TxtAdr2";
            this.TxtAdr2.Size = new System.Drawing.Size(340, 20);
            this.TxtAdr2.TabIndex = 9;
            // 
            // TxtAdr1
            // 
            this.TxtAdr1.Location = new System.Drawing.Point(81, 50);
            this.TxtAdr1.Name = "TxtAdr1";
            this.TxtAdr1.Size = new System.Drawing.Size(340, 20);
            this.TxtAdr1.TabIndex = 8;
            this.TxtAdr1.Text = "Avenue du PLSQL";
            // 
            // TxtPrenom
            // 
            this.TxtPrenom.Location = new System.Drawing.Point(279, 25);
            this.TxtPrenom.Name = "TxtPrenom";
            this.TxtPrenom.Size = new System.Drawing.Size(142, 20);
            this.TxtPrenom.TabIndex = 7;
            this.TxtPrenom.Text = "Patrick";
            // 
            // TxtNom
            // 
            this.TxtNom.Location = new System.Drawing.Point(81, 25);
            this.TxtNom.Name = "TxtNom";
            this.TxtNom.Size = new System.Drawing.Size(142, 20);
            this.TxtNom.TabIndex = 6;
            this.TxtNom.Text = "Dumoulin";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(191, 110);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Ville : ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(35, 107);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "CP : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Adresse";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(229, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Prénom";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nom : ";
            // 
            // GrpIntervenant
            // 
            this.GrpIntervenant.Controls.Add(this.BtnEnregistrerIntervenant);
            this.GrpIntervenant.Controls.Add(this.GrpNuiteIntervenant);
            this.GrpIntervenant.Controls.Add(this.PanFonctionIntervenant);
            this.GrpIntervenant.Controls.Add(this.CmbAtelierIntervenant);
            this.GrpIntervenant.Controls.Add(this.label17);
            this.GrpIntervenant.Location = new System.Drawing.Point(606, 474);
            this.GrpIntervenant.Name = "GrpIntervenant";
            this.GrpIntervenant.Size = new System.Drawing.Size(765, 245);
            this.GrpIntervenant.TabIndex = 25;
            this.GrpIntervenant.TabStop = false;
            this.GrpIntervenant.Text = "Complément Inscription Intervenant";
            this.GrpIntervenant.Visible = false;
            // 
            // BtnEnregistrerIntervenant
            // 
            this.BtnEnregistrerIntervenant.Enabled = false;
            this.BtnEnregistrerIntervenant.Location = new System.Drawing.Point(626, 215);
            this.BtnEnregistrerIntervenant.Name = "BtnEnregistrerIntervenant";
            this.BtnEnregistrerIntervenant.Size = new System.Drawing.Size(133, 25);
            this.BtnEnregistrerIntervenant.TabIndex = 30;
            this.BtnEnregistrerIntervenant.Text = "Enregistrer";
            this.BtnEnregistrerIntervenant.UseVisualStyleBackColor = true;
            this.BtnEnregistrerIntervenant.Click += new System.EventHandler(this.BtnEnregistrerIntervenant_Click);
            // 
            // GrpNuiteIntervenant
            // 
            this.GrpNuiteIntervenant.Controls.Add(this.PanNuiteIntervenant);
            this.GrpNuiteIntervenant.Controls.Add(this.RdbNuiteIntervenantNon);
            this.GrpNuiteIntervenant.Controls.Add(this.RdbNuiteIntervenantOui);
            this.GrpNuiteIntervenant.Location = new System.Drawing.Point(21, 65);
            this.GrpNuiteIntervenant.Name = "GrpNuiteIntervenant";
            this.GrpNuiteIntervenant.Size = new System.Drawing.Size(497, 151);
            this.GrpNuiteIntervenant.TabIndex = 29;
            this.GrpNuiteIntervenant.TabStop = false;
            this.GrpNuiteIntervenant.Text = "Nuités";
            // 
            // PanNuiteIntervenant
            // 
            this.PanNuiteIntervenant.Location = new System.Drawing.Point(3, 43);
            this.PanNuiteIntervenant.Name = "PanNuiteIntervenant";
            this.PanNuiteIntervenant.Size = new System.Drawing.Size(494, 102);
            this.PanNuiteIntervenant.TabIndex = 24;
            this.PanNuiteIntervenant.Visible = false;
            // 
            // RdbNuiteIntervenantNon
            // 
            this.RdbNuiteIntervenantNon.AutoSize = true;
            this.RdbNuiteIntervenantNon.Checked = true;
            this.RdbNuiteIntervenantNon.Location = new System.Drawing.Point(92, 15);
            this.RdbNuiteIntervenantNon.Name = "RdbNuiteIntervenantNon";
            this.RdbNuiteIntervenantNon.Size = new System.Drawing.Size(45, 17);
            this.RdbNuiteIntervenantNon.TabIndex = 23;
            this.RdbNuiteIntervenantNon.TabStop = true;
            this.RdbNuiteIntervenantNon.Text = "Non";
            this.RdbNuiteIntervenantNon.UseVisualStyleBackColor = true;
            this.RdbNuiteIntervenantNon.CheckedChanged += new System.EventHandler(this.RdbNuiteIntervenant_CheckedChanged);
            // 
            // RdbNuiteIntervenantOui
            // 
            this.RdbNuiteIntervenantOui.AutoSize = true;
            this.RdbNuiteIntervenantOui.Location = new System.Drawing.Point(23, 16);
            this.RdbNuiteIntervenantOui.Name = "RdbNuiteIntervenantOui";
            this.RdbNuiteIntervenantOui.Size = new System.Drawing.Size(41, 17);
            this.RdbNuiteIntervenantOui.TabIndex = 22;
            this.RdbNuiteIntervenantOui.Text = "Oui";
            this.RdbNuiteIntervenantOui.UseVisualStyleBackColor = true;
            this.RdbNuiteIntervenantOui.CheckedChanged += new System.EventHandler(this.RdbNuiteIntervenant_CheckedChanged);
            // 
            // PanFonctionIntervenant
            // 
            this.PanFonctionIntervenant.Location = new System.Drawing.Point(305, 4);
            this.PanFonctionIntervenant.Name = "PanFonctionIntervenant";
            this.PanFonctionIntervenant.Size = new System.Drawing.Size(168, 41);
            this.PanFonctionIntervenant.TabIndex = 28;
            // 
            // CmbAtelierIntervenant
            // 
            this.CmbAtelierIntervenant.FormattingEnabled = true;
            this.CmbAtelierIntervenant.Location = new System.Drawing.Point(77, 29);
            this.CmbAtelierIntervenant.Name = "CmbAtelierIntervenant";
            this.CmbAtelierIntervenant.Size = new System.Drawing.Size(218, 21);
            this.CmbAtelierIntervenant.TabIndex = 26;
            this.CmbAtelierIntervenant.TextChanged += new System.EventHandler(this.CmbAtelierIntervenant_TextChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(18, 32);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(45, 13);
            this.label17.TabIndex = 23;
            this.label17.Text = "Atelier : ";
            // 
            // CmdQuitter
            // 
            this.CmdQuitter.Location = new System.Drawing.Point(808, 244);
            this.CmdQuitter.Name = "CmdQuitter";
            this.CmdQuitter.Size = new System.Drawing.Size(144, 36);
            this.CmdQuitter.TabIndex = 22;
            this.CmdQuitter.Text = "Quitter";
            this.CmdQuitter.UseVisualStyleBackColor = true;
            this.CmdQuitter.Click += new System.EventHandler(this.CmdQuitter_Click);
            // 
            // PicAffiche
            // 
            this.PicAffiche.Image = global::MaisonDesLigues.Properties.Resources.affiche;
            this.PicAffiche.Location = new System.Drawing.Point(808, 22);
            this.PicAffiche.Name = "PicAffiche";
            this.PicAffiche.Size = new System.Drawing.Size(144, 216);
            this.PicAffiche.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicAffiche.TabIndex = 5;
            this.PicAffiche.TabStop = false;
            // 
            // TabPrincipal
            // 
            this.TabPrincipal.Controls.Add(this.TabInscription);
            this.TabPrincipal.Controls.Add(this.TabAjout);
            this.TabPrincipal.Controls.Add(this.TabArrive);
            this.TabPrincipal.Location = new System.Drawing.Point(0, 0);
            this.TabPrincipal.Name = "TabPrincipal";
            this.TabPrincipal.SelectedIndex = 0;
            this.TabPrincipal.Size = new System.Drawing.Size(802, 633);
            this.TabPrincipal.TabIndex = 0;
            this.TabPrincipal.Click += new System.EventHandler(this.TabPrincipal_Click);
            // 
            // TabAjout
            // 
            this.TabAjout.Controls.Add(this.GbMajVacation);
            this.TabAjout.Controls.Add(this.GbAjout);
            this.TabAjout.Controls.Add(this.GbVacation);
            this.TabAjout.Controls.Add(this.GbTheme);
            this.TabAjout.Controls.Add(this.GbAtelier);
            this.TabAjout.Location = new System.Drawing.Point(4, 22);
            this.TabAjout.Name = "TabAjout";
            this.TabAjout.Padding = new System.Windows.Forms.Padding(3);
            this.TabAjout.Size = new System.Drawing.Size(794, 607);
            this.TabAjout.TabIndex = 1;
            this.TabAjout.Text = "Organisation";
            this.TabAjout.UseVisualStyleBackColor = true;
            // 
            // GbMajVacation
            // 
            this.GbMajVacation.Controls.Add(this.groupBox1);
            this.GbMajVacation.Controls.Add(this.label27);
            this.GbMajVacation.Controls.Add(this.label26);
            this.GbMajVacation.Controls.Add(this.CbxModificationVac);
            this.GbMajVacation.Controls.Add(this.CbxModifVacation);
            this.GbMajVacation.Controls.Add(this.BtnMaj);
            this.GbMajVacation.Controls.Add(this.label18);
            this.GbMajVacation.Location = new System.Drawing.Point(23, 172);
            this.GbMajVacation.Name = "GbMajVacation";
            this.GbMajVacation.Size = new System.Drawing.Size(765, 175);
            this.GbMajVacation.TabIndex = 12;
            this.GbMajVacation.TabStop = false;
            this.GbMajVacation.Text = "Changement horraire d\'une Vacation";
            this.GbMajVacation.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.DpDateDebutMaj);
            this.groupBox1.Controls.Add(this.DpHeureFinMaj);
            this.groupBox1.Controls.Add(this.DpHeureDebutMaj);
            this.groupBox1.Controls.Add(this.DpDateFinMaj);
            this.groupBox1.Location = new System.Drawing.Point(6, 88);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(365, 77);
            this.groupBox1.TabIndex = 23;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Nouveaux Horraires";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(22, 22);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(81, 13);
            this.label16.TabIndex = 1;
            this.label16.Text = "Date de début :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(22, 54);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(65, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "Date de fin :";
            // 
            // DpDateDebutMaj
            // 
            this.DpDateDebutMaj.CustomFormat = "dd-MM-yyyy ";
            this.DpDateDebutMaj.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DpDateDebutMaj.Location = new System.Drawing.Point(181, 19);
            this.DpDateDebutMaj.Name = "DpDateDebutMaj";
            this.DpDateDebutMaj.Size = new System.Drawing.Size(102, 20);
            this.DpDateDebutMaj.TabIndex = 17;
            // 
            // DpHeureFinMaj
            // 
            this.DpHeureFinMaj.CustomFormat = "HH:mm";
            this.DpHeureFinMaj.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DpHeureFinMaj.Location = new System.Drawing.Point(289, 51);
            this.DpHeureFinMaj.Name = "DpHeureFinMaj";
            this.DpHeureFinMaj.ShowUpDown = true;
            this.DpHeureFinMaj.Size = new System.Drawing.Size(62, 20);
            this.DpHeureFinMaj.TabIndex = 20;
            // 
            // DpHeureDebutMaj
            // 
            this.DpHeureDebutMaj.CustomFormat = "HH:mm";
            this.DpHeureDebutMaj.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DpHeureDebutMaj.Location = new System.Drawing.Point(289, 19);
            this.DpHeureDebutMaj.Name = "DpHeureDebutMaj";
            this.DpHeureDebutMaj.ShowUpDown = true;
            this.DpHeureDebutMaj.Size = new System.Drawing.Size(62, 20);
            this.DpHeureDebutMaj.TabIndex = 18;
            // 
            // DpDateFinMaj
            // 
            this.DpDateFinMaj.CustomFormat = "dd-MM-yyyy ";
            this.DpDateFinMaj.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DpDateFinMaj.Location = new System.Drawing.Point(181, 51);
            this.DpDateFinMaj.Name = "DpDateFinMaj";
            this.DpDateFinMaj.Size = new System.Drawing.Size(102, 20);
            this.DpDateFinMaj.TabIndex = 19;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(17, 55);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(105, 13);
            this.label27.TabIndex = 22;
            this.label27.Text = "Date et heure debut:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(17, 23);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(45, 13);
            this.label26.TabIndex = 21;
            this.label26.Text = "Atelier : ";
            // 
            // CbxModificationVac
            // 
            this.CbxModificationVac.FormattingEnabled = true;
            this.CbxModificationVac.Location = new System.Drawing.Point(149, 52);
            this.CbxModificationVac.Name = "CbxModificationVac";
            this.CbxModificationVac.Size = new System.Drawing.Size(222, 21);
            this.CbxModificationVac.TabIndex = 12;
            this.CbxModificationVac.SelectedIndexChanged += new System.EventHandler(this.CbxModificationVac_SelectedIndexChanged);
            // 
            // CbxModifVacation
            // 
            this.CbxModifVacation.FormattingEnabled = true;
            this.CbxModifVacation.Location = new System.Drawing.Point(149, 20);
            this.CbxModifVacation.Name = "CbxModifVacation";
            this.CbxModifVacation.Size = new System.Drawing.Size(222, 21);
            this.CbxModifVacation.TabIndex = 11;
            this.CbxModifVacation.SelectedIndexChanged += new System.EventHandler(this.CbxModifVacation_SelectedIndexChanged);
            // 
            // BtnMaj
            // 
            this.BtnMaj.Location = new System.Drawing.Point(684, 142);
            this.BtnMaj.Name = "BtnMaj";
            this.BtnMaj.Size = new System.Drawing.Size(75, 23);
            this.BtnMaj.TabIndex = 6;
            this.BtnMaj.Text = "Mise A Jour";
            this.BtnMaj.UseVisualStyleBackColor = true;
            this.BtnMaj.Click += new System.EventHandler(this.BtnMaj_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(17, 47);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(0, 13);
            this.label18.TabIndex = 0;
            // 
            // GbAjout
            // 
            this.GbAjout.Controls.Add(this.RdTheme);
            this.GbAjout.Controls.Add(this.RdAtelier);
            this.GbAjout.Controls.Add(this.RdVacation);
            this.GbAjout.Location = new System.Drawing.Point(23, 6);
            this.GbAjout.Name = "GbAjout";
            this.GbAjout.Size = new System.Drawing.Size(765, 58);
            this.GbAjout.TabIndex = 24;
            this.GbAjout.TabStop = false;
            this.GbAjout.Text = "Type Ajout";
            // 
            // RdTheme
            // 
            this.RdTheme.AutoSize = true;
            this.RdTheme.Location = new System.Drawing.Point(334, 24);
            this.RdTheme.Name = "RdTheme";
            this.RdTheme.Size = new System.Drawing.Size(58, 17);
            this.RdTheme.TabIndex = 23;
            this.RdTheme.TabStop = true;
            this.RdTheme.Text = "Theme";
            this.RdTheme.UseVisualStyleBackColor = true;
            this.RdTheme.CheckedChanged += new System.EventHandler(this.RdTheme_CheckedChanged);
            // 
            // RdAtelier
            // 
            this.RdAtelier.AutoSize = true;
            this.RdAtelier.Location = new System.Drawing.Point(86, 24);
            this.RdAtelier.Name = "RdAtelier";
            this.RdAtelier.Size = new System.Drawing.Size(54, 17);
            this.RdAtelier.TabIndex = 21;
            this.RdAtelier.TabStop = true;
            this.RdAtelier.Text = "Atelier";
            this.RdAtelier.UseVisualStyleBackColor = true;
            this.RdAtelier.CheckedChanged += new System.EventHandler(this.RdAtelier_CheckedChanged);
            // 
            // RdVacation
            // 
            this.RdVacation.AutoSize = true;
            this.RdVacation.Location = new System.Drawing.Point(592, 24);
            this.RdVacation.Name = "RdVacation";
            this.RdVacation.Size = new System.Drawing.Size(67, 17);
            this.RdVacation.TabIndex = 22;
            this.RdVacation.TabStop = true;
            this.RdVacation.Text = "Vacation";
            this.RdVacation.UseVisualStyleBackColor = true;
            this.RdVacation.CheckedChanged += new System.EventHandler(this.RdVacation_CheckedChanged);
            // 
            // GbVacation
            // 
            this.GbVacation.Controls.Add(this.DpHeureFin);
            this.GbVacation.Controls.Add(this.DpDateFin);
            this.GbVacation.Controls.Add(this.DpHeureDebut);
            this.GbVacation.Controls.Add(this.DpDateDebut);
            this.GbVacation.Controls.Add(this.CbAtelierVacation);
            this.GbVacation.Controls.Add(this.label13);
            this.GbVacation.Controls.Add(this.BtnAjoutVacation);
            this.GbVacation.Controls.Add(this.label12);
            this.GbVacation.Controls.Add(this.label11);
            this.GbVacation.Location = new System.Drawing.Point(23, 456);
            this.GbVacation.Name = "GbVacation";
            this.GbVacation.Size = new System.Drawing.Size(765, 126);
            this.GbVacation.TabIndex = 2;
            this.GbVacation.TabStop = false;
            this.GbVacation.Text = "ajout d\'une Vacation";
            this.GbVacation.Visible = false;
            // 
            // DpHeureFin
            // 
            this.DpHeureFin.CustomFormat = "HH:mm";
            this.DpHeureFin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DpHeureFin.Location = new System.Drawing.Point(257, 53);
            this.DpHeureFin.Name = "DpHeureFin";
            this.DpHeureFin.ShowUpDown = true;
            this.DpHeureFin.Size = new System.Drawing.Size(62, 20);
            this.DpHeureFin.TabIndex = 16;
            // 
            // DpDateFin
            // 
            this.DpDateFin.CustomFormat = "dd-MM-yyyy ";
            this.DpDateFin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DpDateFin.Location = new System.Drawing.Point(149, 53);
            this.DpDateFin.Name = "DpDateFin";
            this.DpDateFin.Size = new System.Drawing.Size(102, 20);
            this.DpDateFin.TabIndex = 15;
            // 
            // DpHeureDebut
            // 
            this.DpHeureDebut.CustomFormat = "HH:mm";
            this.DpHeureDebut.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DpHeureDebut.Location = new System.Drawing.Point(257, 21);
            this.DpHeureDebut.Name = "DpHeureDebut";
            this.DpHeureDebut.ShowUpDown = true;
            this.DpHeureDebut.Size = new System.Drawing.Size(62, 20);
            this.DpHeureDebut.TabIndex = 14;
            // 
            // DpDateDebut
            // 
            this.DpDateDebut.CustomFormat = "dd-MM-yyyy ";
            this.DpDateDebut.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DpDateDebut.Location = new System.Drawing.Point(149, 21);
            this.DpDateDebut.Name = "DpDateDebut";
            this.DpDateDebut.Size = new System.Drawing.Size(102, 20);
            this.DpDateDebut.TabIndex = 13;
            // 
            // CbAtelierVacation
            // 
            this.CbAtelierVacation.FormattingEnabled = true;
            this.CbAtelierVacation.Location = new System.Drawing.Point(149, 84);
            this.CbAtelierVacation.Name = "CbAtelierVacation";
            this.CbAtelierVacation.Size = new System.Drawing.Size(170, 21);
            this.CbAtelierVacation.TabIndex = 9;
            this.CbAtelierVacation.SelectedIndexChanged += new System.EventHandler(this.CbAtelierVacation_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(15, 87);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(48, 13);
            this.label13.TabIndex = 8;
            this.label13.Text = " Atelier : ";
            // 
            // BtnAjoutVacation
            // 
            this.BtnAjoutVacation.Location = new System.Drawing.Point(684, 97);
            this.BtnAjoutVacation.Name = "BtnAjoutVacation";
            this.BtnAjoutVacation.Size = new System.Drawing.Size(75, 23);
            this.BtnAjoutVacation.TabIndex = 6;
            this.BtnAjoutVacation.Text = "Ajouter";
            this.BtnAjoutVacation.UseVisualStyleBackColor = true;
            this.BtnAjoutVacation.Click += new System.EventHandler(this.BtnAjoutVacation_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(17, 59);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "Date de fin :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(17, 27);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(81, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Date de début :";
            // 
            // GbTheme
            // 
            this.GbTheme.Controls.Add(this.CmbAtelierAjoutTheme);
            this.GbTheme.Controls.Add(this.label10);
            this.GbTheme.Controls.Add(this.BtnAjoutTheme);
            this.GbTheme.Controls.Add(this.txtBoxNomTheme);
            this.GbTheme.Controls.Add(this.labelNom);
            this.GbTheme.Location = new System.Drawing.Point(23, 353);
            this.GbTheme.Name = "GbTheme";
            this.GbTheme.Size = new System.Drawing.Size(765, 97);
            this.GbTheme.TabIndex = 1;
            this.GbTheme.TabStop = false;
            this.GbTheme.Text = "Ajout d\'un Thème";
            this.GbTheme.Visible = false;
            // 
            // CmbAtelierAjoutTheme
            // 
            this.CmbAtelierAjoutTheme.FormattingEnabled = true;
            this.CmbAtelierAjoutTheme.Location = new System.Drawing.Point(149, 53);
            this.CmbAtelierAjoutTheme.Name = "CmbAtelierAjoutTheme";
            this.CmbAtelierAjoutTheme.Size = new System.Drawing.Size(170, 21);
            this.CmbAtelierAjoutTheme.TabIndex = 6;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 56);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 13);
            this.label10.TabIndex = 5;
            this.label10.Text = " Atelier : ";
            // 
            // BtnAjoutTheme
            // 
            this.BtnAjoutTheme.Location = new System.Drawing.Point(684, 67);
            this.BtnAjoutTheme.Name = "BtnAjoutTheme";
            this.BtnAjoutTheme.Size = new System.Drawing.Size(75, 23);
            this.BtnAjoutTheme.TabIndex = 4;
            this.BtnAjoutTheme.Text = "Ajouter";
            this.BtnAjoutTheme.UseVisualStyleBackColor = true;
            this.BtnAjoutTheme.Click += new System.EventHandler(this.BtnAjoutTheme_Click);
            // 
            // txtBoxNomTheme
            // 
            this.txtBoxNomTheme.Location = new System.Drawing.Point(149, 23);
            this.txtBoxNomTheme.Name = "txtBoxNomTheme";
            this.txtBoxNomTheme.Size = new System.Drawing.Size(170, 20);
            this.txtBoxNomTheme.TabIndex = 3;
            // 
            // labelNom
            // 
            this.labelNom.AutoSize = true;
            this.labelNom.Location = new System.Drawing.Point(17, 26);
            this.labelNom.Name = "labelNom";
            this.labelNom.Size = new System.Drawing.Size(67, 13);
            this.labelNom.TabIndex = 1;
            this.labelNom.Text = "Nom thème :";
            // 
            // GbAtelier
            // 
            this.GbAtelier.Controls.Add(this.BtnAjoutAtelier);
            this.GbAtelier.Controls.Add(this.labelNbPlaceMax);
            this.GbAtelier.Controls.Add(this.numUpDownNbPlace);
            this.GbAtelier.Controls.Add(this.txtBoxNomAtelier);
            this.GbAtelier.Controls.Add(this.labelNomAtelier);
            this.GbAtelier.Location = new System.Drawing.Point(23, 70);
            this.GbAtelier.Name = "GbAtelier";
            this.GbAtelier.Size = new System.Drawing.Size(765, 85);
            this.GbAtelier.TabIndex = 0;
            this.GbAtelier.TabStop = false;
            this.GbAtelier.Text = "Ajout d\'Atelier";
            this.GbAtelier.Visible = false;
            // 
            // BtnAjoutAtelier
            // 
            this.BtnAjoutAtelier.Location = new System.Drawing.Point(684, 55);
            this.BtnAjoutAtelier.Name = "BtnAjoutAtelier";
            this.BtnAjoutAtelier.Size = new System.Drawing.Size(75, 23);
            this.BtnAjoutAtelier.TabIndex = 6;
            this.BtnAjoutAtelier.Text = "Ajouter";
            this.BtnAjoutAtelier.UseVisualStyleBackColor = true;
            this.BtnAjoutAtelier.Click += new System.EventHandler(this.BtnAjoutAtelier_Click);
            // 
            // labelNbPlaceMax
            // 
            this.labelNbPlaceMax.AutoSize = true;
            this.labelNbPlaceMax.Location = new System.Drawing.Point(17, 55);
            this.labelNbPlaceMax.Name = "labelNbPlaceMax";
            this.labelNbPlaceMax.Size = new System.Drawing.Size(108, 13);
            this.labelNbPlaceMax.TabIndex = 3;
            this.labelNbPlaceMax.Text = "Nombre places maxi :";
            // 
            // numUpDownNbPlace
            // 
            this.numUpDownNbPlace.Location = new System.Drawing.Point(149, 53);
            this.numUpDownNbPlace.Name = "numUpDownNbPlace";
            this.numUpDownNbPlace.Size = new System.Drawing.Size(54, 20);
            this.numUpDownNbPlace.TabIndex = 2;
            // 
            // txtBoxNomAtelier
            // 
            this.txtBoxNomAtelier.Location = new System.Drawing.Point(149, 19);
            this.txtBoxNomAtelier.MaxLength = 50;
            this.txtBoxNomAtelier.Name = "txtBoxNomAtelier";
            this.txtBoxNomAtelier.Size = new System.Drawing.Size(222, 20);
            this.txtBoxNomAtelier.TabIndex = 1;
            // 
            // labelNomAtelier
            // 
            this.labelNomAtelier.AutoSize = true;
            this.labelNomAtelier.Location = new System.Drawing.Point(17, 22);
            this.labelNomAtelier.Name = "labelNomAtelier";
            this.labelNomAtelier.Size = new System.Drawing.Size(66, 13);
            this.labelNomAtelier.TabIndex = 0;
            this.labelNomAtelier.Text = "Nom atelier :";
            // 
            // TabArrive
            // 
            this.TabArrive.Controls.Add(this.GrpHeureArriver);
            this.TabArrive.Location = new System.Drawing.Point(4, 22);
            this.TabArrive.Name = "TabArrive";
            this.TabArrive.Padding = new System.Windows.Forms.Padding(3);
            this.TabArrive.Size = new System.Drawing.Size(794, 607);
            this.TabArrive.TabIndex = 2;
            this.TabArrive.Text = "Heure Arrivée";
            this.TabArrive.UseVisualStyleBackColor = true;
            // 
            // GrpHeureArriver
            // 
            this.GrpHeureArriver.Controls.Add(this.BtnValiderHeureArriver);
            this.GrpHeureArriver.Controls.Add(this.DtpDateArriver);
            this.GrpHeureArriver.Controls.Add(this.CmbParticipant);
            this.GrpHeureArriver.Controls.Add(this.DtpHeureArriver);
            this.GrpHeureArriver.Location = new System.Drawing.Point(6, 6);
            this.GrpHeureArriver.Name = "GrpHeureArriver";
            this.GrpHeureArriver.Size = new System.Drawing.Size(381, 100);
            this.GrpHeureArriver.TabIndex = 17;
            this.GrpHeureArriver.TabStop = false;
            this.GrpHeureArriver.Text = "Heure d\'arriver du participant";
            // 
            // BtnValiderHeureArriver
            // 
            this.BtnValiderHeureArriver.Location = new System.Drawing.Point(269, 69);
            this.BtnValiderHeureArriver.Name = "BtnValiderHeureArriver";
            this.BtnValiderHeureArriver.Size = new System.Drawing.Size(103, 25);
            this.BtnValiderHeureArriver.TabIndex = 23;
            this.BtnValiderHeureArriver.Text = "Valider";
            this.BtnValiderHeureArriver.UseVisualStyleBackColor = true;
            this.BtnValiderHeureArriver.Click += new System.EventHandler(this.BtnValiderHeureArriver_Click);
            // 
            // DtpDateArriver
            // 
            this.DtpDateArriver.CustomFormat = "dd-MM-yyyy ";
            this.DtpDateArriver.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpDateArriver.Location = new System.Drawing.Point(202, 26);
            this.DtpDateArriver.Name = "DtpDateArriver";
            this.DtpDateArriver.Size = new System.Drawing.Size(102, 20);
            this.DtpDateArriver.TabIndex = 14;
            // 
            // CmbParticipant
            // 
            this.CmbParticipant.FormattingEnabled = true;
            this.CmbParticipant.Location = new System.Drawing.Point(12, 25);
            this.CmbParticipant.Name = "CmbParticipant";
            this.CmbParticipant.Size = new System.Drawing.Size(184, 21);
            this.CmbParticipant.TabIndex = 16;
            // 
            // DtpHeureArriver
            // 
            this.DtpHeureArriver.CustomFormat = "HH:mm";
            this.DtpHeureArriver.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpHeureArriver.Location = new System.Drawing.Point(310, 26);
            this.DtpHeureArriver.Name = "DtpHeureArriver";
            this.DtpHeureArriver.ShowUpDown = true;
            this.DtpHeureArriver.Size = new System.Drawing.Size(62, 20);
            this.DtpHeureArriver.TabIndex = 15;
            // 
            // FrmPrincipale
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(964, 632);
            this.Controls.Add(this.TabPrincipal);
            this.Controls.Add(this.CmdQuitter);
            this.Controls.Add(this.PicAffiche);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmPrincipale";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmPrincipale_FormClosing);
            this.Load += new System.EventHandler(this.FrmPrincipale_Load);
            this.TabInscription.ResumeLayout(false);
            this.GrpLicencie.ResumeLayout(false);
            this.GrpLicencie.PerformLayout();
            this.GrbAccompagant.ResumeLayout(false);
            this.GrbAccompagant.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NudValChequeAcc)).EndInit();
            this.GrpPaiemantLicencie.ResumeLayout(false);
            this.GrpPaiemantLicencie.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NudValeurChequeLicencieTout)).EndInit();
            this.GrpNuiteLicencie.ResumeLayout(false);
            this.GrpNuiteLicencie.PerformLayout();
            this.GrpBenevole.ResumeLayout(false);
            this.GrpBenevole.PerformLayout();
            this.GrpTypeParticipant.ResumeLayout(false);
            this.GrpTypeParticipant.PerformLayout();
            this.GrpIdentite.ResumeLayout(false);
            this.GrpIdentite.PerformLayout();
            this.GrpIntervenant.ResumeLayout(false);
            this.GrpIntervenant.PerformLayout();
            this.GrpNuiteIntervenant.ResumeLayout(false);
            this.GrpNuiteIntervenant.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicAffiche)).EndInit();
            this.TabPrincipal.ResumeLayout(false);
            this.TabAjout.ResumeLayout(false);
            this.GbMajVacation.ResumeLayout(false);
            this.GbMajVacation.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.GbAjout.ResumeLayout(false);
            this.GbAjout.PerformLayout();
            this.GbVacation.ResumeLayout(false);
            this.GbVacation.PerformLayout();
            this.GbTheme.ResumeLayout(false);
            this.GbTheme.PerformLayout();
            this.GbAtelier.ResumeLayout(false);
            this.GbAtelier.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDownNbPlace)).EndInit();
            this.TabArrive.ResumeLayout(false);
            this.GrpHeureArriver.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage TabInscription;
        private System.Windows.Forms.GroupBox GrpBenevole;
        private System.Windows.Forms.Button BtnEnregistreBenevole;
        private System.Windows.Forms.Panel PanelDispoBenevole;
        private System.Windows.Forms.MaskedTextBox TxtLicenceBenevole;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MaskedTextBox TxtDateNaissance;
        private System.Windows.Forms.Button CmdQuitter;
        private System.Windows.Forms.GroupBox GrpTypeParticipant;
        private System.Windows.Forms.RadioButton RadLicencie;
        private System.Windows.Forms.RadioButton RadBenevole;
        private System.Windows.Forms.RadioButton RadIntervenant;
        private System.Windows.Forms.GroupBox GrpIdentite;
        private System.Windows.Forms.TextBox TxtMail;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.MaskedTextBox txtTel;
        private System.Windows.Forms.MaskedTextBox TxtCp;
        private System.Windows.Forms.TextBox TxtVille;
        private System.Windows.Forms.TextBox TxtAdr2;
        private System.Windows.Forms.TextBox TxtAdr1;
        private System.Windows.Forms.TextBox TxtPrenom;
        private System.Windows.Forms.TextBox TxtNom;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox PicAffiche;
        private System.Windows.Forms.GroupBox GrpIntervenant;
        private System.Windows.Forms.Button BtnEnregistrerIntervenant;
        private System.Windows.Forms.GroupBox GrpNuiteIntervenant;
        private System.Windows.Forms.Panel PanNuiteIntervenant;
        private System.Windows.Forms.RadioButton RdbNuiteIntervenantNon;
        private System.Windows.Forms.RadioButton RdbNuiteIntervenantOui;
        private System.Windows.Forms.Panel PanFonctionIntervenant;
        private System.Windows.Forms.ComboBox CmbAtelierIntervenant;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TabControl TabPrincipal;
        private System.Windows.Forms.TabPage TabAjout;
        private System.Windows.Forms.GroupBox GbVacation;
        private System.Windows.Forms.GroupBox GbTheme;
        private System.Windows.Forms.GroupBox GbAtelier;
        private System.Windows.Forms.TextBox txtBoxNomAtelier;
        private System.Windows.Forms.Label labelNomAtelier;
        private System.Windows.Forms.Label labelNbPlaceMax;
        private System.Windows.Forms.NumericUpDown numUpDownNbPlace;
        private System.Windows.Forms.TextBox txtBoxNomTheme;
        private System.Windows.Forms.Label labelNom;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button BtnAjoutVacation;
        private System.Windows.Forms.Button BtnAjoutTheme;
        private System.Windows.Forms.Button BtnAjoutAtelier;
        private System.Windows.Forms.ComboBox CmbAtelierAjoutTheme;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox GbAjout;
        private System.Windows.Forms.RadioButton RdTheme;
        private System.Windows.Forms.RadioButton RdVacation;
        private System.Windows.Forms.RadioButton RdAtelier;
        private System.Windows.Forms.ComboBox CbAtelierVacation;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox GbMajVacation;
        private System.Windows.Forms.ComboBox CbxModifVacation;
        private System.Windows.Forms.Button BtnMaj;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.GroupBox GrpLicencie;
        private System.Windows.Forms.Button BtnEnregistrerLicencie;
        private System.Windows.Forms.MaskedTextBox TxtNumLicenceLicencie;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox CbxModificationVac;
        private System.Windows.Forms.DateTimePicker DpDateDebut;
        private System.Windows.Forms.DateTimePicker DpHeureDebut;
        private System.Windows.Forms.DateTimePicker DpHeureFin;
        private System.Windows.Forms.DateTimePicker DpDateFin;
        private System.Windows.Forms.DateTimePicker DpHeureFinMaj;
        private System.Windows.Forms.DateTimePicker DpDateFinMaj;
        private System.Windows.Forms.DateTimePicker DpHeureDebutMaj;
        private System.Windows.Forms.DateTimePicker DpDateDebutMaj;
        private System.Windows.Forms.GroupBox GrpNuiteLicencie;
        private System.Windows.Forms.Panel PanNuiteLicencie;
        private System.Windows.Forms.RadioButton RdbNuiteLicencieNon;
        private System.Windows.Forms.RadioButton RdbNuiteLicencieOui;
        private System.Windows.Forms.ComboBox CbxQualiteLicencie;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.GroupBox GrpPaiemantLicencie;
        private System.Windows.Forms.RadioButton RdbSeparement;
        private System.Windows.Forms.RadioButton RdbTout;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.MaskedTextBox TxtNumChequeLicencieTotal;
        private System.Windows.Forms.NumericUpDown NudValeurChequeLicencieTout;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ListBox LbxAtelierLicencie;
        private System.Windows.Forms.TabPage TabArrive;
        private System.Windows.Forms.ComboBox CmbParticipant;
        private System.Windows.Forms.DateTimePicker DtpHeureArriver;
        private System.Windows.Forms.DateTimePicker DtpDateArriver;
        private System.Windows.Forms.GroupBox GrpHeureArriver;
        private System.Windows.Forms.Button BtnValiderHeureArriver;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox GrbAccompagant;
        private System.Windows.Forms.CheckBox ChkDimancheMidi;
        private System.Windows.Forms.CheckBox ChkSamediSoir;
        private System.Windows.Forms.CheckBox ChkSamediMidi;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.MaskedTextBox maskedTextBox2;
        private System.Windows.Forms.NumericUpDown NudValChequeAcc;
        private System.Windows.Forms.Label label28;
    }
}