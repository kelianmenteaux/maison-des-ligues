﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Configuration;
using System.Collections.ObjectModel;
using ComposantNuite;
using BaseDeDonnees;
using QRCoder;

namespace MaisonDesLigues
{
    public partial class FrmPrincipale : Form
    {

        /// <summary>
        /// constructeur du formulaire
        /// </summary>
        public FrmPrincipale()
        {
            InitializeComponent();
        }
        private Bdd UneConnexion;
        private String TitreApplication;
        private String IdStatutSelectionne = "";
        /// <summary>
        /// création et ouverture d'une connexion vers la base de données sur le chargement du formulaire
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmPrincipale_Load(object sender, EventArgs e)
        {
            UneConnexion = ((FrmLogin)Owner).UneConnexion;
            TitreApplication = ((FrmLogin)Owner).TitreApplication;
            this.Text = TitreApplication;
        }


        private void RadTypeParticipant_Changed(object sender, EventArgs e)
        {
            switch (((RadioButton)sender).Name)
            {
                case "RadBenevole":
                    this.GererInscriptionBenevole();
                    break;
                case "RadLicencie":
                    this.GererInscriptionLicencie();
                    break;
                case "RadIntervenant":
                    this.GererInscriptionIntervenant();
                    break;
                default:
                    throw new Exception("Erreur interne à l'application");
            }
        }

        /// <summary>     
        /// procédure permettant d'afficher l'interface de saisie du complément d'inscription d'un intervenant.
        /// </summary>
        private void GererInscriptionIntervenant()
        {

            GrpBenevole.Visible = false;
            GrpIntervenant.Visible = true;
            PanFonctionIntervenant.Visible = true;
            GrpLicencie.Visible = false;
            GrpIntervenant.Left = 23;
            GrpIntervenant.Top = 264;
            Utilitaire.CreerDesControles(this, UneConnexion, "VSTATUT01", "Rad_", PanFonctionIntervenant, "RadioButton", this.rdbStatutIntervenant_StateChanged);
            Utilitaire.RemplirComboBox(UneConnexion, CmbAtelierIntervenant, "VATELIER01");

            CmbAtelierIntervenant.Text = "Choisir";
        }

        /// <summary>
        /// procédure permettant d'afficher l'interface de saisie d'info sup des Licencié.
        /// </summary>
        private void GererInscriptionLicencie()
        {
            GrpLicencie.Visible = true;
            GrpLicencie.Left = 23;
            GrpLicencie.Top = 264;
            GrpIntervenant.Visible = false;
            GrpBenevole.Visible = false;
            Utilitaire.RemplirComboBox(UneConnexion, CbxQualiteLicencie, "VQUALITE01");
            Utilitaire.RemplirListBox(UneConnexion, LbxAtelierLicencie, "VATELIER01");

            //TODO manque la sweet
        }

        /// <summary>     
        /// procédure permettant d'afficher l'interface de saisie des disponibilités des bénévoles.
        /// </summary>
        private void GererInscriptionBenevole()
        {

            GrpBenevole.Visible = true;
            GrpBenevole.Left = 23;
            GrpBenevole.Top = 264;
            GrpIntervenant.Visible = false;
            GrpLicencie.Visible = false;

            Utilitaire.CreerDesControles(this, UneConnexion, "VDATEBENEVOLAT01", "ChkDateB_", PanelDispoBenevole, "CheckBox", this.rdbStatutIntervenant_StateChanged);
            // on va tester si le controle à placer est de type CheckBox afin de lui placer un événement checked_changed
            // Ceci afin de désactiver les boutons si aucune case à cocher du container n'est cochée
            foreach (Control UnControle in PanelDispoBenevole.Controls)
            {
                if (UnControle.GetType().Name == "CheckBox")
                {
                    CheckBox UneCheckBox = (CheckBox)UnControle;
                    UneCheckBox.CheckedChanged += new System.EventHandler(this.ChkDateBenevole_CheckedChanged);
                }
            }


        }
        /// <summary>
        /// permet d'appeler la méthode VerifBtnEnregistreIntervenant qui déterminera le statu du bouton BtnEnregistrerIntervenant
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdbStatutIntervenant_StateChanged(object sender, EventArgs e)
        {
            // stocke dans un membre de niveau form l'identifiant du statut sélectionné (voir règle de nommage des noms des controles : prefixe_Id)
            this.IdStatutSelectionne = ((RadioButton)sender).Name.Split('_')[1];
            BtnEnregistrerIntervenant.Enabled = VerifBtnEnregistreIntervenant();
        }
        /// <summary>
        /// Permet d'intercepter le click sur le bouton d'enregistrement d'un bénévole.
        /// Cetteméthode va appeler la méthode InscrireBenevole de la Bdd, après avoir mis en forme certains paramètres à envoyer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnEnregistreBenevole_Click(object sender, EventArgs e)
        {
            Collection<Int16> IdDatesSelectionnees = new Collection<Int16>();
            Int64? NumeroLicence;
            if (TxtLicenceBenevole.MaskCompleted)
            {
                NumeroLicence = System.Convert.ToInt64(TxtLicenceBenevole.Text);
            }
            else
            {
                NumeroLicence = null;
            }


            foreach (Control UnControle in PanelDispoBenevole.Controls)
            {
                if (UnControle.GetType().Name == "CheckBox" && ((CheckBox)UnControle).Checked)
                {
                    /* Un name de controle est toujours formé come ceci : xxx_Id où id représente l'id dans la table
                     * Donc on splite la chaine et on récupére le deuxième élément qui correspond à l'id de l'élément sélectionné.
                     * on rajoute cet id dans la collection des id des dates sélectionnées
                        
                    */
                    IdDatesSelectionnees.Add(System.Convert.ToInt16((UnControle.Name.Split('_'))[1]));
                }
            }
            UneConnexion.InscrireBenevole(TxtNom.Text, TxtPrenom.Text, TxtAdr1.Text, TxtAdr2.Text != "" ? TxtAdr2.Text : null, TxtCp.Text, TxtVille.Text, txtTel.MaskCompleted ? txtTel.Text : null, TxtMail.Text != "" ? TxtMail.Text : null, System.Convert.ToDateTime(TxtDateNaissance.Text), NumeroLicence, IdDatesSelectionnees);

            Collection<GroupBox> DesPanels = new Collection<GroupBox>();
            DesPanels.Add(GrpIdentite);
            DesPanels.Add(GrpBenevole);
            ViderChamps(DesPanels);
        }
        /// <summary>
        /// Cetet méthode teste les données saisies afin d'activer ou désactiver le bouton d'enregistrement d'un bénévole
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChkDateBenevole_CheckedChanged(object sender, EventArgs e)
        {
            BtnEnregistreBenevole.Enabled = (TxtLicenceBenevole.Text == "" || TxtLicenceBenevole.MaskCompleted) && TxtDateNaissance.MaskCompleted && Utilitaire.CompteChecked(PanelDispoBenevole) > 0;
        }
        /// <summary>
        /// Méthode qui permet d'afficher ou masquer le controle panel permettant la saisie des nuités d'un intervenant.
        /// S'il faut rendre visible le panel, on teste si les nuités possibles ont été chargés dans ce panel. Si non, on les charges 
        /// On charge ici autant de contrôles ResaNuit qu'il y a de nuits possibles
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RdbNuiteIntervenant_CheckedChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Name == "RdbNuiteIntervenantOui")
            {
                PanNuiteIntervenant.Visible = true;
                if (PanNuiteIntervenant.Controls.Count == 0) // on charge les nuites possibles et on les affiche
                {
                    //DataTable LesDateNuites = UneConnexion.ObtenirDonnesOracle("VDATENUITE01");
                    //foreach(Dat
                    Dictionary<Int16, String> LesNuites = UneConnexion.ObtenirDatesNuites();
                    int i = 0;
                    foreach (KeyValuePair<Int16, String> UneNuite in LesNuites)
                    {
                        ComposantNuite.ResaNuite unResaNuit = new ResaNuite(UneConnexion.ObtenirDonnesOracle("VHOTEL01"), (UneConnexion.ObtenirDonnesOracle("VCATEGORIECHAMBRE01")), UneNuite.Value, UneNuite.Key);
                        unResaNuit.Left = 5;
                        unResaNuit.Top = 5 + (24 * i++);
                        unResaNuit.Visible = true;
                        //unResaNuit.click += new System.EventHandler(ComposantNuite_StateChanged);
                        PanNuiteIntervenant.Controls.Add(unResaNuit);
                    }

                }

            }
            else
            {
                PanNuiteIntervenant.Visible = false;

            }
            BtnEnregistrerIntervenant.Enabled = VerifBtnEnregistreIntervenant();

        }

        /// <summary>
        /// Cette procédure va appeler la procédure .... qui aura pour but d'enregistrer les éléments 
        /// de l'inscription d'un intervenant, avec éventuellment les nuités à prendre en compte        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnEnregistrerIntervenant_Click(object sender, EventArgs e)
        {
            try
            {
                if (RdbNuiteIntervenantOui.Checked)
                {
                    // inscription avec les nuitées
                    Collection<Int16> NuitsSelectionnes = new Collection<Int16>();
                    Collection<String> HotelsSelectionnes = new Collection<String>();
                    Collection<String> CategoriesSelectionnees = new Collection<string>();
                    foreach (Control UnControle in PanNuiteIntervenant.Controls)
                    {
                        if (UnControle.GetType().Name == "ResaNuite" && ((ResaNuite)UnControle).GetNuitSelectionnee())
                        {
                            // la nuité a été cochée, il faut donc envoyer l'hotel et la type de chambre à la procédure de la base qui va enregistrer le contenu hébergement 
                            //ContenuUnHebergement UnContenuUnHebergement= new ContenuUnHebergement();
                            CategoriesSelectionnees.Add(((ResaNuite)UnControle).GetTypeChambreSelectionnee());
                            HotelsSelectionnes.Add(((ResaNuite)UnControle).GetHotelSelectionne());
                            NuitsSelectionnes.Add(((ResaNuite)UnControle).IdNuite);
                        }

                    }
                    if (NuitsSelectionnes.Count == 0)
                    {
                        MessageBox.Show("Si vous avez sélectionné que l'intervenant avait des nuités\n in faut qu'au moins une nuit soit sélectionnée");
                    }
                    else
                    {
                        UneConnexion.InscrireIntervenant(TxtNom.Text, TxtPrenom.Text, TxtAdr1.Text, TxtAdr2.Text != "" ? TxtAdr2.Text : null, TxtCp.Text, TxtVille.Text, txtTel.MaskCompleted ? txtTel.Text : null, TxtMail.Text != "" ? TxtMail.Text : null, System.Convert.ToInt16(CmbAtelierIntervenant.SelectedValue), this.IdStatutSelectionne, CategoriesSelectionnees, HotelsSelectionnes, NuitsSelectionnes);
                        MessageBox.Show("Inscription intervenant effectuée");

                        Collection<GroupBox> DesPanels = new Collection<GroupBox>();
                        DesPanels.Add(GrpIdentite);
                        DesPanels.Add(GrpIntervenant);
                        ViderChamps(DesPanels);
                    }
                }
                else
                { // inscription sans les nuitées
                    UneConnexion.InscrireIntervenant(TxtNom.Text, TxtPrenom.Text, TxtAdr1.Text, TxtAdr2.Text != "" ? TxtAdr2.Text : null, TxtCp.Text, TxtVille.Text, txtTel.MaskCompleted ? txtTel.Text : null, TxtMail.Text != "" ? TxtMail.Text : null, System.Convert.ToInt16(CmbAtelierIntervenant.SelectedValue), this.IdStatutSelectionne);
                    MessageBox.Show("Inscription intervenant effectuée");

                    Collection<GroupBox> DesPanels = new Collection<GroupBox>();
                    DesPanels.Add(GrpIdentite);
                    DesPanels.Add(GrpIntervenant);
                    ViderChamps(DesPanels);
                }


            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        /// <summary>
        /// Méthode privée testant le contrôle combo et la variable IdStatutSelectionne qui contient une valeur
        /// Cette méthode permetra ensuite de définir l'état du bouton BtnEnregistrerIntervenant
        /// </summary>
        /// <returns></returns>
        private Boolean VerifBtnEnregistreIntervenant()
        {
            return CmbAtelierIntervenant.Text != "Choisir" && this.IdStatutSelectionne.Length > 0;
        }
        /// <summary>
        /// Méthode permettant de définir le statut activé/désactivé du bouton BtnEnregistrerIntervenant
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>"
        private void CmbAtelierIntervenant_TextChanged(object sender, EventArgs e)
        {
            BtnEnregistrerIntervenant.Enabled = VerifBtnEnregistreIntervenant();
        }

        //ajout d'un atelier
        private void BtnAjoutAtelier_Click(object sender, EventArgs e)
        {
            UneConnexion.AjouterAtelier(txtBoxNomAtelier.Text, (int)numUpDownNbPlace.Value);
        }

        //ajout d'un theme, créer sequence pour le numero?
        private void BtnAjoutTheme_Click(object sender, EventArgs e)
        {
            if (CmbAtelierAjoutTheme.Text!= " - Choisir un atelier - ")
            {
                UneConnexion.AjouterTheme(Convert.ToInt16(CmbAtelierAjoutTheme.SelectedValue), txtBoxNomTheme.Text);
            }
            else
            {
                MessageBox.Show("selectionner un atelier");
            }
            
        }

        //temporaire
        private void TabPrincipal_Click(object sender, EventArgs e)
        {
            try
            {   
                //arriver
                Utilitaire.RemplirComboBox(UneConnexion, CmbParticipant, "VPARTICIPANT01");
                
            }
            catch (Exception ex)
            {
            }

        }
        /// <summary>
        /// gére l'affichage pour vacation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RdVacation_CheckedChanged(object sender, EventArgs e)
        {
            GbAtelier.Visible = false;
            GbTheme.Visible = false;
            GbVacation.Visible = true;
            GbMajVacation.Visible = true;

            GbVacation.Top = 71;
            GbVacation.Left = 23;
            GbMajVacation.Left = 23;
            GbMajVacation.Top = GbVacation.Height + 71 + 10 ;

            Utilitaire.RemplirComboBox(UneConnexion, CbAtelierVacation, "VATELIER01");
            Utilitaire.RemplirComboBox(UneConnexion, CbxModifVacation, "VVACATION02");
            CbxModifVacation.Text = " - Choisir un atelier - ";
            CbxModificationVac.Text = " - Selectionner un horraire - ";
        }
        /// <summary>
        /// gére l'affichage pour theme
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RdTheme_CheckedChanged(object sender, EventArgs e)
        {
            GbAtelier.Visible = false;
            GbTheme.Visible = true;
            GbVacation.Visible = false;
            GbMajVacation.Visible = false;

            GbTheme.Top = 71;
            GbTheme.Left = 23;
            Utilitaire.RemplirComboBox(UneConnexion, CmbAtelierAjoutTheme, "VATELIER01");
            CmbAtelierAjoutTheme.Text = " - Choisir un atelier - ";
        }
        /// <summary>
        /// gére l'affichage pour atelier
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RdAtelier_CheckedChanged(object sender, EventArgs e)
        {
            GbAtelier.Visible = true;
            GbTheme.Visible = false;
            GbVacation.Visible = false;
            GbMajVacation.Visible = false;

            GbAtelier.Top = 71;
            GbAtelier.Left = 23;
        }


//
        /// <summary>
        /// gestion de l'événement click du bouton quitter.
        /// Demande de confirmation avant de quitetr l'application.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdQuitter_Click(object sender, EventArgs e)
        {
            AskConfirmQuitAppli();
        }
        private void FrmPrincipale_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }
        // demande au user confirmation pour quitter, 
        // renvoie true si confirmé
        private void AskConfirmQuitAppli()
        {
            // message confirmation quitter l'application

            if (MessageBox.Show("Voulez-vous quitter l'application ?",
                               ConfigurationManager.AppSettings["TitreApplication"],
                               MessageBoxButtons.YesNo) == DialogResult.No)
            {
                // non

            }
            else
            {
                Application.Exit();
            }
            
        }  
//



        /// <summary>
        /// Methode permettant de vider les champs des groupbox d'une collection passé en parametre.
        /// </summary>
        /// <param name="LesGroupBoxs"></param>
        public static void ViderChamps(Collection<GroupBox> LesGroupBoxs)
        {
            foreach (GroupBox uneGroupBox in LesGroupBoxs)
            {
                foreach (Object unObjet in uneGroupBox.Controls)
                {
                    if (unObjet.GetType().Name == "TextBox")
                    {
                        ((TextBox)unObjet).Text = "";
                    }
                    else if (unObjet.GetType().Name == "ComboBox")
                    {
                        ((ComboBox)unObjet).SelectedIndex = -1;
                    }
                    else if (unObjet.GetType().Name == "RadioButton")
                    {
                        ((RadioButton)unObjet).Checked= false;
                    }
                    else if (unObjet.GetType().Name == "CheckBox")
                    {
                        ((CheckBox)unObjet).Checked = false;
                    }
                    else if(unObjet.GetType().Name == "MaskedTextBox"){
                        ((MaskedTextBox)unObjet).Text = null;
                    }
                    else if (unObjet.GetType().Name == "Button")
                    {
                        ((Button)unObjet).Enabled = false;
                    }
                    
                }
            }
        }
    

        /// <summary>
        /// appelle la fonction de creation de vacation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAjoutVacation_Click(object sender, EventArgs e)
        {
            UneConnexion.AjouterVacation(Convert.ToInt16(CbAtelierVacation.SelectedValue), System.Convert.ToDateTime(DpDateDebut.Text + DpHeureDebut.Text), System.Convert.ToDateTime(DpDateFin.Text + DpHeureFin.Text));
        }

        private void CbAtelierVacation_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// appelle la fonction de mise a jour d'une vacation avec en parametre les different champ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnMaj_Click(object sender, EventArgs e)
        {
            if (CbxModifVacation.Text != " - Choisir un atelier - " || CbxModificationVac.Text!= " - Selectionner un horraire - ")
            {
                UneConnexion.MajVacation(Convert.ToInt16(CbxModifVacation.SelectedValue), Convert.ToInt16(CbxModificationVac.SelectedValue), System.Convert.ToDateTime(DpDateDebutMaj.Text + DpHeureDebutMaj.Text), System.Convert.ToDateTime(DpDateFinMaj.Text + DpHeureFinMaj.Text));
            }
            else
            {
                MessageBox.Show("Veillez choisir un atelier et une vacation");
            }
            
        }

        /// <summary>
        /// remplie la 2eme combobox de maj vacation avec des date de vacation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CbxModifVacation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CbxModifVacation.SelectedValue is Int32)
            {
                //Utilitaire.RemplirComboBoxVac(UneConnexion, CbxModificationVac, "VVACATION01", Int16.Parse(CbxModifVacation.SelectedValue.ToString()));
                DataTable Lesvacations = UneConnexion.ObtenirVacationsAtelier(System.Convert.ToInt16(CbxModifVacation.SelectedValue));

                CbxModificationVac.Refresh();
                CbxModificationVac.DataSource =Lesvacations;
 
                CbxModificationVac.DisplayMember = "libelle";
                CbxModificationVac.ValueMember= "id";
            }
        }

        /// <summary>
        /// declenche l'enregistrement licencie en fonction des different champ rempli/selectionné
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnEnregistrerLicencie_Click(object sender, EventArgs e)
        {
            try
            {
                string lesNumCheques = TxtNumChequeLicencieTotal.Text;
                Int16 lesMontCheques = Convert.ToInt16(NudValeurChequeLicencieTout.Value);
                string typePaiement = "Tout";
                
                Collection<Int16> AteliersSelectionnes = new Collection<Int16>();
                // Récupération des ateliers séléctionnés par l'utilisateur, et ajout dans la collection AteliersSelectionnes
                foreach (DataRowView unObjet in LbxAtelierLicencie.SelectedItems)
                {
                    AteliersSelectionnes.Add(System.Convert.ToInt16(unObjet.Row.ItemArray[0]));
                }

                if (RdbNuiteLicencieOui.Checked)
                {
                    // inscription avec les nuitées
                    Collection<Int16> NuitsSelectionnes = new Collection<Int16>();
                    Collection<String> HotelsSelectionnes = new Collection<String>();
                    Collection<String> CategoriesSelectionnees = new Collection<String>();
                    foreach (Control UnControle in PanNuiteLicencie.Controls)
                    {
                        if (UnControle.GetType().Name == "ResaNuite" && ((ResaNuite)UnControle).GetNuitSelectionnee())
                        {
                            // la nuité a été cochée, il faut donc envoyer l'hotel et la type de chambre à la procédure de la base qui va enregistrer le contenu hébergement 
                            //ContenuUnHebergement UnContenuUnHebergement= new ContenuUnHebergement();
                            CategoriesSelectionnees.Add(((ResaNuite)UnControle).GetTypeChambreSelectionnee());
                            HotelsSelectionnes.Add(((ResaNuite)UnControle).GetHotelSelectionne());
                            NuitsSelectionnes.Add(((ResaNuite)UnControle).IdNuite);
                        }

                    }
                    if (NuitsSelectionnes.Count == 0)
                    {
                        MessageBox.Show("Si vous avez sélectionné que licencié avait des nuités\n in faut qu'au moins une nuit soit sélectionnée");
                    }
                    else
                    {
                        UneConnexion.InscrireLicencie(TxtNom.Text, TxtPrenom.Text, TxtAdr1.Text, TxtAdr2.Text != "" ? TxtAdr2.Text : null, TxtCp.Text, TxtVille.Text, txtTel.MaskCompleted ? txtTel.Text : null, TxtMail.Text != "" ? TxtMail.Text : null, Convert.ToInt64(TxtNumLicenceLicencie.Text), Convert.ToInt16(CbxQualiteLicencie.SelectedValue), lesNumCheques, lesMontCheques, typePaiement, AteliersSelectionnes, CategoriesSelectionnees, HotelsSelectionnes, NuitsSelectionnes);
                        MessageBox.Show("Inscription licencié effectuée");


                        Collection<GroupBox> DesPanels = new Collection<GroupBox>();
                        DesPanels.Add(GrpIdentite);
                        DesPanels.Add(GrpLicencie);
                        ViderChamps(DesPanels);
                    }
                }
                else
                { // inscription sans les nuitées
                    UneConnexion.InscrireLicencie(TxtNom.Text, TxtPrenom.Text, TxtAdr1.Text, TxtAdr2.Text != "" ? TxtAdr2.Text : null, TxtCp.Text, TxtVille.Text, txtTel.MaskCompleted ? txtTel.Text : null, TxtMail.Text != "" ? TxtMail.Text : null, Convert.ToInt64(TxtNumLicenceLicencie.Text), Convert.ToInt16(CbxQualiteLicencie.SelectedValue),  lesNumCheques, lesMontCheques, typePaiement,AteliersSelectionnes);
                    MessageBox.Show("Inscription licencié effectuée");

                    Collection<GroupBox> DesPanels = new Collection<GroupBox>();
                    DesPanels.Add(GrpIdentite);
                    DesPanels.Add(GrpLicencie);
                    ViderChamps(DesPanels);
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        /// <summary>
        /// gere l'affichage des nuité dans licencié
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RdbNuiteLicencie_CheckedChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Name == "RdbNuiteLicencieOui")
            {
                PanNuiteLicencie.Visible = true;
                if (PanNuiteLicencie.Controls.Count == 0) // on charge les nuites possibles et on les affiche
                {
                    //DataTable LesDateNuites = UneConnexion.ObtenirDonnesOracle("VDATENUITE01");
                    //foreach(Dat
                    Dictionary<Int16, String> LesNuites = UneConnexion.ObtenirDatesNuites();
                    int i = 0;
                    foreach (KeyValuePair<Int16, String> UneNuite in LesNuites)
                    {
                        ComposantNuite.ResaNuite unResaNuit = new ResaNuite(UneConnexion.ObtenirDonnesOracle("VHOTEL01"), (UneConnexion.ObtenirDonnesOracle("VCATEGORIECHAMBRE01")), UneNuite.Value, UneNuite.Key);
                        unResaNuit.Left = 5;
                        unResaNuit.Top = 5 + (24 * i++);
                        unResaNuit.Visible = true;
                        //unResaNuit.click += new System.EventHandler(ComposantNuite_StateChanged);
                        PanNuiteLicencie.Controls.Add(unResaNuit);
                    }

                }

            }
            else
            {
                PanNuiteLicencie.Visible = false;

            }
            BtnEnregistrerLicencie.Enabled = VerifBtnEnregistreIntervenant();
        }


        /// <summary>
        /// verifie que max 5 atelier soit selectionner dans inscription licencié.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LbxAtelierLicencie_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (LbxAtelierLicencie.SelectedItems.Count >5)
            {
                LbxAtelierLicencie.SetSelected(LbxAtelierLicencie.SelectedIndex, false);
            }
        }

        /// <summary>
        /// appelle enregistrerArriver, envois l'id participant, la date arriver et une clé wifi
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnValiderHeureArriver_Click(object sender, EventArgs e)
        {

            UneConnexion.EnregistrerArriver(Convert.ToInt16(CmbParticipant.SelectedValue), System.Convert.ToDateTime(DtpDateArriver.Text + DtpHeureArriver.Text), Utilitaire.CreerCleWifi());
        }



        /// <summary>
        /// charge les date des vacation dans les datepicker, pour la vacation choisi.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CbxModificationVac_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CbxModificationVac.Text!= "System.Data.DataRowView")
            {
                string[] mesDates = CbxModificationVac.Text.Split('.');
                DpDateDebutMaj.Value = Convert.ToDateTime(mesDates[0]);
                DpHeureDebutMaj.Value = Convert.ToDateTime(mesDates[0]);

                DpDateFinMaj.Value = Convert.ToDateTime(mesDates[1]);
                DpHeureFinMaj.Value = Convert.ToDateTime(mesDates[1]);
            }

        }


        /// <summary>
        ///verifie si licence licencie remplie
        /// </summary>
        /// <returns></returns>
        private Boolean VerifBtnEnregistreLicencie()
        {
            return TxtNumLicenceLicencie.Text != "";
        }

        private void CbxQualiteLicencie_SelectedIndexChanged(object sender, EventArgs e)
        {
            BtnEnregistrerLicencie.Enabled = VerifBtnEnregistreLicencie();
        }

        private void TxtNumLicenceLicencie_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {
            BtnEnregistrerLicencie.Enabled = VerifBtnEnregistreLicencie();
        }

        /// <summary>
        /// methode conne , car deja a moitie dans utilitaire , permet de calculer la valeur du cheque accompagant
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChkAccompagantRepas_CheckedChanged(object sender, EventArgs e)
        {
            int PrixRepas = 35; // il fausrai recupérer la valeur en base
            int total = 0;
            foreach (Control Crtl in GrbAccompagant.Controls)
            {
                if ((Crtl is CheckBox) && ((CheckBox)Crtl).Checked)
                {
                    total = total + PrixRepas;
                }
            }
            NudValChequeAcc.Value = total;
        }

        /// <summary>
        /// gere l'activation/ desactivation de groupe box accompagant
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RdbSeparement_CheckedChanged(object sender, EventArgs e)
        {
                GrbAccompagant.Enabled = RdbSeparement.Checked;
        }


    }
    
}
