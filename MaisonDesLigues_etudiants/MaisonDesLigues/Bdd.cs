﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Oracle.DataAccess.Client;
using System.Configuration;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;  // bibliothèque pour les expressions régulières
using MaisonDesLigues;
using System.Net.Mail;
using System.Net;

namespace BaseDeDonnees
{
    class Bdd
    {
        //
        // propriétés membres
        //
        private OracleConnection CnOracle;
        private OracleCommand UneOracleCommand;
        private OracleDataAdapter UnOracleDataAdapter;
        private DataTable UneDataTable;
        private OracleTransaction UneOracleTransaction;
        //
        // méthodes
        //
        /// <summary>
        /// constructeur de la connexion 
        /// </summary>
        /// <param name="UnLogin">login utilisateur</param>
        /// <param name="UnPwd">mot de passe utilisateur</param>
        public Bdd(String UnLogin, String UnPwd)
        {
            try
            {
                /// <remarks>on commence par récupérer dans CnString les informations contenues dans le fichier app.config
                /// pour la connectionString de nom StrConnMdl
                /// </remarks>
                ConnectionStringSettings CnString = ConfigurationManager.ConnectionStrings["StrConnMdl"];
                ///<remarks>
                /// on va remplacer dans la chaine de connexion les paramètres par le login et le pwd saisis
                ///dans les zones de texte. Pour ça on va utiliser la méthode Format de la classe String.                /// 
                /// </remarks>
                CnOracle = new OracleConnection(string.Format(CnString.ConnectionString, UnLogin, UnPwd));
                CnOracle.Open();
            }
            catch (OracleException)
            {
                try
                {
                    ConnectionStringSettings CnString = ConfigurationManager.ConnectionStrings["StrConnHome"];
                    CnOracle = new OracleConnection(string.Format(CnString.ConnectionString, UnLogin, UnPwd));
                    CnOracle.Open();

                }
                catch (OracleException Oex)
                {
                    throw new Exception("Erreur à la connexion" + Oex.Message);

                }
            }
        }         
                
            
        
        
        /// <summary>
        /// Méthode permettant de fermer la connexion
        /// </summary>
        public void FermerConnexion()
        {
            this.CnOracle.Close();
        }
        /// <summary>
        /// méthode permettant de renvoyer un message d'erreur provenant de la bd
        /// après l'avoir formatté. On ne renvoie que le message, sans code erreur
        /// </summary>
        /// <param name="unMessage">message à formater</param>
        /// <returns>message formaté à afficher dans l'application</returns>
        private String GetMessageOracle(String unMessage)
        {
            String[] message = Regex.Split(unMessage, "ORA-");
            return (Regex.Split(message[1], ":"))[1];
        }
        /// <summary>
        /// permet de récupérer le contenu d'une table ou d'une vue. 
        /// </summary>
        /// <param name="UneTableOuVue"> nom de la table ou la vue dont on veut récupérer le contenu</param>
        /// <returns>un objet de type datatable contenant les données récupérées</returns>
        public DataTable ObtenirDonnesOracle(String UneTableOuVue)
        {
            string Sql = "select * from " + UneTableOuVue;
            this.UneOracleCommand = new OracleCommand(Sql, CnOracle);
            UnOracleDataAdapter = new OracleDataAdapter();
            UnOracleDataAdapter.SelectCommand = this.UneOracleCommand;
            UneDataTable = new DataTable();
            UnOracleDataAdapter.Fill(UneDataTable);
            return UneDataTable;
        }

        public DataTable ObtenirDonnesOracle(String UneTableOuVue, int idatelier)
        {
            string Sql = "select * from " + UneTableOuVue + " where id =" + idatelier ;
            this.UneOracleCommand = new OracleCommand(Sql, CnOracle);
            UnOracleDataAdapter = new OracleDataAdapter();
            UnOracleDataAdapter.SelectCommand = this.UneOracleCommand;
            UneDataTable = new DataTable();
            UnOracleDataAdapter.Fill(UneDataTable);
            return UneDataTable;
        }
        /// <summary>
        /// méthode privée permettant de valoriser les paramètres d'un objet commmand communs aux licenciés, bénévoles et intervenants ainsi que l'envois d'un mail de confirmation.
        /// </summary>
        /// <param name="Cmd">nom de l'objet command concerné par les paramètres</param>
        /// <param name="pNom">nom du participant</param>
        /// <param name="pPrenom">prénom du participant</param>
        /// <param name="pAdresse1">adresse1 du participant</param>
        /// <param name="pAdresse2">adresse2 du participant</param>
        /// <param name="pCp">cp du participant</param>
        /// <param name="pVille">ville du participant</param>
        /// <param name="pTel">téléphone du participant</param>
        /// <param name="pMail">mail du participant</param>
        private void ParamCommunsNouveauxParticipants(OracleCommand Cmd, String pNom, String pPrenom, String pAdresse1, String pAdresse2, String pCp, String pVille, String pTel, String pMail)
        {
            Cmd.Parameters.Add("pNom", OracleDbType.Varchar2, ParameterDirection.Input).Value = pNom;
            Cmd.Parameters.Add("pPrenom", OracleDbType.Varchar2, ParameterDirection.Input).Value = pPrenom;
            Cmd.Parameters.Add("pAdr1", OracleDbType.Varchar2, ParameterDirection.Input).Value = pAdresse1;
            Cmd.Parameters.Add("pAdr2", OracleDbType.Varchar2, ParameterDirection.Input).Value = pAdresse2;
            Cmd.Parameters.Add("pCp", OracleDbType.Varchar2, ParameterDirection.Input).Value = pCp;
            Cmd.Parameters.Add("pVille", OracleDbType.Varchar2, ParameterDirection.Input).Value = pVille;
            Cmd.Parameters.Add("pTel", OracleDbType.Varchar2, ParameterDirection.Input).Value = pTel;
            Cmd.Parameters.Add("pMail", OracleDbType.Varchar2, ParameterDirection.Input).Value = pMail;
            if (pMail!=null) EnvoisMail(pMail);
        }
        /// <summary>
        /// procédure qui va se charger d'invoquer la procédure stockée qui ira inscrire un participant de type bénévole
        /// </summary>
        /// <param name="Cmd">nom de l'objet command concerné par les paramètres</param>
        /// <param name="pNom">nom du participant</param>
        /// <param name="pPrenom">prénom du participant</param>
        /// <param name="pAdresse1">adresse1 du participant</param>
        /// <param name="pAdresse2">adresse2 du participant</param>
        /// <param name="pCp">cp du participant</param>
        /// <param name="pVille">ville du participant</param>
        /// <param name="pTel">téléphone du participant</param>
        /// <param name="pMail">mail du participant</param>
        /// <param name="pDateNaissance">mail du bénévole</param>
        /// <param name="pNumeroLicence">numéro de licence du bénévole ou null</param>
        /// <param name="pDateBenevolat">collection des id des dates où le bénévole sera présent</param>
        public void InscrireBenevole(String pNom, String pPrenom, String pAdresse1, String pAdresse2, String pCp, String pVille, String pTel, String pMail, DateTime pDateNaissance, Int64? pNumeroLicence, Collection<Int16> pDateBenevolat)
        {
            try
            {
                UneOracleCommand = new OracleCommand("pckparticipant.nouveaubenevole", CnOracle);
                UneOracleCommand.CommandType = CommandType.StoredProcedure;
                this.ParamCommunsNouveauxParticipants(UneOracleCommand, pNom, pPrenom, pAdresse1, pAdresse2, pCp, pVille, pTel, pMail);
                UneOracleCommand.Parameters.Add("pDateNaiss", OracleDbType.Date, ParameterDirection.Input).Value = pDateNaissance;
                UneOracleCommand.Parameters.Add("pLicence", OracleDbType.Int64, ParameterDirection.Input).Value = pNumeroLicence;
                //UneOracleCommand.Parameters.Add("pLesDates", OracleDbType.Array, ParameterDirection.Input).Value = pDateBenevolat;
                OracleParameter pLesDates = new OracleParameter();
                pLesDates.ParameterName = "pLesDates";
                pLesDates.OracleDbType = OracleDbType.Int16;
                pLesDates.CollectionType = OracleCollectionType.PLSQLAssociativeArray;

                pLesDates.Value = pDateBenevolat.ToArray();
                pLesDates.Size = pDateBenevolat.Count;
                UneOracleCommand.Parameters.Add(pLesDates);
                UneOracleCommand.ExecuteNonQuery();
                MessageBox.Show("inscription bénévole effectuée");
            }
            catch (OracleException Oex)
            {
                MessageBox.Show("Erreur Oracle \n" + Oex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Autre Erreur  \n" + ex.Message);
            }

        }
        /// <summary>
        /// méthode privée permettant de valoriser les paramètres d'un objet commmand spécifiques intervenants
        /// </summary>
        /// <param name="Cmd"> nom de l'objet command concerné par les paramètres</param>
        /// <param name="pIdAtelier"> Id de l'atelier où interviendra l'intervenant</param>
        /// <param name="pIdStatut">statut de l'intervenant pour l'atelier : animateur ou intervenant</param>
        private void ParamsSpecifiquesIntervenant(OracleCommand Cmd,Int16 pIdAtelier, String pIdStatut)
        {
            Cmd.Parameters.Add("pIdAtelier", OracleDbType.Int16, ParameterDirection.Input).Value = pIdAtelier;
            Cmd.Parameters.Add("pIdStatut", OracleDbType.Char, ParameterDirection.Input).Value = pIdStatut;
        }
        /// <summary>
        /// Procédure publique qui va appeler la procédure stockée permettant d'inscrire un nouvel intervenant sans nuité
        /// </summary>
        /// <param name="Cmd">nom de l'objet command concerné par les paramètres</param>
        /// <param name="pNom">nom du participant</param>
        /// <param name="pPrenom">prénom du participant</param>
        /// <param name="pAdresse1">adresse1 du participant</param>
        /// <param name="pAdresse2">adresse2 du participant</param>
        /// <param name="pCp">cp du participant</param>
        /// <param name="pVille">ville du participant</param>
        /// <param name="pTel">téléphone du participant</param>
        /// <param name="pMail">mail du participant</param>
        /// <param name="pIdAtelier"> Id de l'atelier où interviendra l'intervenant</param>
        /// <param name="pIdStatut">statut de l'intervenant pour l'atelier : animateur ou intervenant</param>
        public void InscrireIntervenant(String pNom, String pPrenom, String pAdresse1, String pAdresse2, String pCp, String pVille, String pTel, String pMail, Int16 pIdAtelier, String pIdStatut)
        {
            /// <remarks>
            /// procédure qui va créer :
            /// 1- un enregistrement dans la table participant
            /// 2- un enregistrement dans la table intervenant 
            ///  en cas d'erreur Oracle, appel à la méthode GetMessageOracle dont le rôle est d'extraire uniquement le message renvoyé
            /// par une procédure ou un trigger Oracle
            /// </remarks>
            /// 
            String MessageErreur = "";
            try
            {
                UneOracleCommand = new OracleCommand("pckparticipant.nouvelintervenant", CnOracle);
                UneOracleCommand.CommandType = CommandType.StoredProcedure;
                // début de la transaction Oracle il vaut mieyx gérer les transactions dans l'applicatif que dans la bd dans les procédures stockées.
                UneOracleTransaction = this.CnOracle.BeginTransaction();
                // on appelle la procédure ParamCommunsNouveauxParticipants pour charger les paramètres communs aux intervenants
                this.ParamCommunsNouveauxParticipants(UneOracleCommand, pNom, pPrenom, pAdresse1, pAdresse2, pCp, pVille, pTel, pMail);
                // on appelle la procédure ParamsCommunsIntervenant pour charger les paramètres communs aux intervenants
                this.ParamsSpecifiquesIntervenant(UneOracleCommand, pIdAtelier, pIdStatut);        
                //execution
                UneOracleCommand.ExecuteNonQuery();
                // fin de la transaction. Si on arrive à ce point, c'est qu'aucune exception n'a été levée
                UneOracleTransaction.Commit();
            }
            catch (OracleException Oex)
            {
                MessageErreur = "Erreur Oracle \n" + this.GetMessageOracle(Oex.Message);
             }
            catch (Exception ex)
            {

                MessageErreur = "Autre Erreur, les informations n'ont pas été correctement saisies";
            }
            finally
            {
                if (MessageErreur.Length > 0)
                {
                    // annulation de la transaction
                    UneOracleTransaction.Rollback();
                    // Déclenchement de l'exception
                    throw new Exception(MessageErreur);
                }
            }
        }
        /// <summary>
        /// Procédure publique qui va appeler la procédure stockée permettant d'inscrire un nouvel intervenant qui aura des nuités
        /// </summary>
        /// <param name="Cmd">nom de l'objet command concerné par les paramètres</param>
        /// <param name="pNom">nom du participant</param>
        /// <param name="pPrenom">prénom du participant</param>
        /// <param name="pAdresse1">adresse1 du participant</param>
        /// <param name="pAdresse2">adresse2 du participant</param>
        /// <param name="pCp">cp du participant</param>
        /// <param name="pVille">ville du participant</param>
        /// <param name="pTel">téléphone du participant</param>
        /// <param name="pMail">mail du participant</param>
        /// <param name="pIdAtelier"> Id de l'atelier où interviendra l'intervenant</param>
        /// <param name="pIdStatut">statut de l'intervenant pour l'atelier : animateur ou intervenant</param>
        /// <param name="pLesCategories">tableau contenant la catégorie de chambre pour chaque nuité à réserver</param>
        /// <param name="pLesHotels">tableau contenant l'hôtel pour chaque nuité à réserver</param>
        /// <param name="pLesNuits">tableau contenant l'id de la date d'arrivée pour chaque nuité à réserver</param>
        public void InscrireIntervenant(String pNom, String pPrenom, String pAdresse1, String pAdresse2, String pCp, String pVille, String pTel, String pMail, Int16 pIdAtelier, String pIdStatut, Collection<string> pLesCategories, Collection<string> pLesHotels, Collection<Int16>pLesNuits)
        {
            /// <remarks>
            /// procédure qui va  :
            /// 1- faire appel à la procédure 
            /// un enregistrement dans la table participant
            /// 2- un enregistrement dans la table intervenant 
            /// 3- un à 2 enregistrements dans la table CONTENUHEBERGEMENT
            /// 
            /// en cas d'erreur Oracle, appel à la méthode GetMessageOracle dont le rôle est d'extraire uniquement le message renvoyé
            /// par une procédure ou un trigger Oracle
            /// </remarks>
            /// 
            String MessageErreur="";
            try
            {                
                // pckparticipant.nouvelintervenant est une procédure surchargée
                UneOracleCommand = new OracleCommand("pckparticipant.nouvelintervenant", CnOracle);
                UneOracleCommand.CommandType = CommandType.StoredProcedure;
                // début de la transaction Oracle : il vaut mieyx gérer les transactions dans l'applicatif que dans la bd.
                UneOracleTransaction = this.CnOracle.BeginTransaction();
                this.ParamCommunsNouveauxParticipants(UneOracleCommand, pNom, pPrenom, pAdresse1, pAdresse2, pCp, pVille, pTel, pMail);
                this.ParamsSpecifiquesIntervenant(UneOracleCommand, pIdAtelier, pIdStatut);

                //On va créer ici les paramètres spécifiques à l'inscription d'un intervenant qui réserve des nuits d'hôtel.
                // Paramètre qui stocke les catégories sélectionnées
                OracleParameter pOraLescategories = new OracleParameter();
                pOraLescategories.ParameterName = "pLesCategories";
                pOraLescategories.OracleDbType = OracleDbType.Char;
                pOraLescategories.CollectionType = OracleCollectionType.PLSQLAssociativeArray;
                pOraLescategories.Value = pLesCategories.ToArray();
                pOraLescategories.Size = pLesCategories.Count;
                UneOracleCommand.Parameters.Add(pOraLescategories);
               
                // Paramètre qui stocke les hotels sélectionnées
                OracleParameter pOraLesHotels = new OracleParameter();
                pOraLesHotels.ParameterName = "pLesHotels";
                pOraLesHotels.OracleDbType = OracleDbType.Char;
                pOraLesHotels.CollectionType = OracleCollectionType.PLSQLAssociativeArray;
                pOraLesHotels.Value = pLesHotels.ToArray();
                pOraLesHotels.Size = pLesHotels.Count;
                UneOracleCommand.Parameters.Add(pOraLesHotels);
                
                // Paramètres qui stocke les nuits sélectionnées
                OracleParameter pOraLesNuits = new OracleParameter();
                pOraLesNuits.ParameterName = "pLesNuits";
                pOraLesNuits.OracleDbType = OracleDbType.Int16;
                pOraLesNuits.CollectionType = OracleCollectionType.PLSQLAssociativeArray;
                pOraLesNuits.Value = pLesNuits.ToArray();
                pOraLesNuits.Size = pLesNuits.Count;
                UneOracleCommand.Parameters.Add(pOraLesNuits);
                //execution
                UneOracleCommand.ExecuteNonQuery();
                // fin de la transaction. Si on arrive à ce point, c'est qu'aucune exception n'a été levée
                UneOracleTransaction.Commit();
               
            }
            catch (OracleException Oex)
            {
                //MessageErreur="Erreur Oracle \n" + this.GetMessageOracle(Oex.Message);
                MessageBox.Show(Oex.Message);
            }
            catch (Exception ex)
            {
                
                MessageErreur= "Autre Erreur, les informations n'ont pas été correctement saisies";
            }
            finally
            {
                if (MessageErreur.Length > 0)
                {
                    // annulation de la transaction
                    UneOracleTransaction.Rollback();
                    // Déclenchement de l'exception
                    throw new Exception(MessageErreur);
                }             
            }
        }
        /// <summary>
        /// fonction permettant de construire un dictionnaire dont l'id est l'id d'une nuité et le contenu une date
        /// sous la la forme : lundi 7 janvier 2013        /// 
        /// </summary>
        /// <returns>un dictionnaire dont l'id est l'id d'une nuité et le contenu une date</returns>
        public Dictionary<Int16, String> ObtenirDatesNuites()
        {
            Dictionary<Int16, String> LesDatesARetourner = new Dictionary<Int16, String>();
            DataTable LesDatesNuites = this.ObtenirDonnesOracle("VDATENUITE01");
            foreach (DataRow UneLigne in LesDatesNuites.Rows)
            {
                LesDatesARetourner.Add(System.Convert.ToInt16(UneLigne["id"]), UneLigne["libelle"].ToString());
            }
            return LesDatesARetourner;

        }

        /// <summary>
        /// fonction permettant l'ajout d'un theme dans la table theme
        /// </summary>
        /// <param name="pIdAtelier"></param>
        /// <param name="pNumTheme"></param>
        /// <param name="pLibelle"></param>
        public void AjouterTheme(int pIdAtelier, string pLibelle)
        {
            try
            {
                UneOracleCommand = new OracleCommand("pkg_gestion_ui.cree_theme", CnOracle);
                UneOracleCommand.CommandType = CommandType.StoredProcedure;

                
                UneOracleCommand.Parameters.Add("p_IdAtelier", OracleDbType.Int16, ParameterDirection.Input).Value = pIdAtelier;
                UneOracleCommand.Parameters.Add("p_LibelleTheme", OracleDbType.Varchar2, ParameterDirection.Input).Value = pLibelle;

                UneOracleCommand.ExecuteNonQuery();
                MessageBox.Show("Ajout theme effectué");
            }
            catch (OracleException Oex)
            {
                MessageBox.Show("Erreur Oracle \n" + Oex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Autre Erreur  \n" + ex.Message);
            }
        }
        /// <summary>
        /// fonction permettant l'ajout d'un atelier dans la table atelier
        /// </summary>
        /// <param name="pLibelle"></param>
        /// <param name="pNbPlace"></param>
        public void AjouterAtelier(string pLibelle, int pNbPlace)
        {
            try
            {
                UneOracleCommand = new OracleCommand("pkg_gestion_ui.cree_atelier", CnOracle);
                UneOracleCommand.CommandType = CommandType.StoredProcedure;

                UneOracleCommand.Parameters.Add("p_LibelleAtelier", OracleDbType.Varchar2, ParameterDirection.Input).Value = pLibelle;
                UneOracleCommand.Parameters.Add("p_NbPlacesMaxi", OracleDbType.Int64, ParameterDirection.Input).Value = pNbPlace;

                UneOracleCommand.ExecuteNonQuery();
                MessageBox.Show("Ajout atelier effectué");
            }
            catch (OracleException Oex)
            {
                MessageBox.Show("Erreur Oracle \n" + Oex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Autre Erreur  \n" + ex.Message);
            }
        }
        /// <summary>
        /// fonction permettant l'ajout d'une vacation dans la table vacation
        /// </summary>
        /// <param name="pAtelier"></param>
        /// <param name="pDateDebut"></param>
        /// <param name="pDateFin"></param>
        public void AjouterVacation(int pAtelier, DateTime pDateDebut, DateTime pDateFin)
        {
            try
            {
                UneOracleCommand = new OracleCommand("pkg_gestion_ui.cree_vacation", CnOracle);
                UneOracleCommand.CommandType = CommandType.StoredProcedure;

                UneOracleCommand.Parameters.Add("p_IdAtelier", OracleDbType.Int64, ParameterDirection.Input).Value = pAtelier;
                UneOracleCommand.Parameters.Add("p_HeureDebut", OracleDbType.Date, ParameterDirection.Input).Value = pDateDebut;
                UneOracleCommand.Parameters.Add("p_HeureFin", OracleDbType.Date, ParameterDirection.Input).Value = pDateFin;
                UneOracleCommand.ExecuteNonQuery();
                MessageBox.Show("Ajout Vacation effectué");
            }
            catch (OracleException Oex)
            {
                MessageBox.Show("Erreur Oracle \n" + Oex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Autre Erreur  \n" + ex.Message);
            }
        }
        /// <summary>
        /// fonction permettant la modification d'une vacation dans la table vacation (uniquement les dates sont modifiables)
        /// </summary>
        /// <param name="pAtelier"></param>
        /// <param name="pNbVac"></param>
        /// <param name="pDateDebut"></param>
        /// <param name="pDateFin"></param>
        public void MajVacation(int pAtelier, int pNbVac, DateTime pDateDebut, DateTime pDateFin)
        {
            try
            {
                UneOracleCommand = new OracleCommand("pkg_gestion_ui.maj_vacation", CnOracle);
                UneOracleCommand.CommandType = CommandType.StoredProcedure;

                UneOracleCommand.Parameters.Add("p_IdAtelier", OracleDbType.Int64, ParameterDirection.Input).Value = pAtelier;
                UneOracleCommand.Parameters.Add("p_Numero", OracleDbType.Int64, ParameterDirection.Input).Value = pNbVac;
                UneOracleCommand.Parameters.Add("p_HeureDebut", OracleDbType.Date, ParameterDirection.Input).Value = pDateDebut;
                UneOracleCommand.Parameters.Add("p_HeureFin", OracleDbType.Date, ParameterDirection.Input).Value = pDateFin;
                UneOracleCommand.ExecuteNonQuery();
                MessageBox.Show("modification Vacation effectué");
            }
            catch (OracleException Oex)
            {
                MessageBox.Show("Erreur Oracle \n" + Oex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Autre Erreur  \n" + ex.Message);
            }
        }
        /// <summary>
        /// procédure permettant de récupérer toutes les vacations d'un atelier
        /// </summary>
        /// <param name="pUnCritere"></param>
        /// <returns></returns>
        public DataTable ObtenirVacationsAtelier(Int32 pUnCritere)
        {
            String Sql = "select numero as id,concat(concat(to_char(heuredebut,'dd-MM-yyyy  HH24:MI') ,' . '), to_char(heurefin,'dd-MM-yyyy  HH24:MI'))as libelle from vvacation01 where idatelier = :pidatelier order by numero ";
            this.UneOracleCommand = new OracleCommand(Sql, CnOracle);
            UneOracleCommand.Parameters.Add("pidatelier", OracleDbType.Int16, ParameterDirection.Input).Value = pUnCritere;
            UnOracleDataAdapter = new OracleDataAdapter();
            UnOracleDataAdapter.SelectCommand = this.UneOracleCommand;
            UneDataTable = new DataTable();
            UnOracleDataAdapter.Fill(UneDataTable);
            return UneDataTable;

        }



        /// <summary>
        /// méthode privée permettant de valoriser les paramètres d'un objet commmand spécifiques intervenants
        /// </summary>
        /// <param name="Cmd"> nom de l'objet command concerné par les paramètres</param>
        /// <param name="pIdAtelier"> Id de l'atelier où interviendra l'intervenant</param>
        /// <param name="pIdStatut">statut de l'intervenant pour l'atelier : animateur ou intervenant</param>
        private void ParamSpecifiquesLicencie(OracleCommand UneOracleCommand, Int64 pLicence, Int16 pQualite, String pNumCheque, Int16 pMontantCheque, String pTypePaiement, Collection<Int16> pLesAteliers)
        {
            UneOracleCommand.Parameters.Add("pLicence", OracleDbType.Int64, ParameterDirection.Input).Value = pLicence;
            UneOracleCommand.Parameters.Add("pQualite", OracleDbType.Int16, ParameterDirection.Input).Value = pQualite;
            UneOracleCommand.Parameters.Add("pNumCheque", OracleDbType.Int64, ParameterDirection.Input).Value = pNumCheque;
            UneOracleCommand.Parameters.Add("pMontantCheque", OracleDbType.Int16, ParameterDirection.Input).Value = pMontantCheque;
            UneOracleCommand.Parameters.Add("pTypePaiement", OracleDbType.Char, ParameterDirection.Input).Value = pTypePaiement;

            // Les ateliers
            OracleParameter pOraLesAteliers = new OracleParameter();
            pOraLesAteliers.ParameterName = "pLesAteliers";
            pOraLesAteliers.OracleDbType = OracleDbType.Int16;
            pOraLesAteliers.CollectionType = OracleCollectionType.PLSQLAssociativeArray;
            pOraLesAteliers.Value = pLesAteliers.ToArray();
            pOraLesAteliers.Size = pLesAteliers.Count;
            UneOracleCommand.Parameters.Add(pOraLesAteliers);
        }
        /// <summary>
        /// Ajoute l'heure d'arriver et la cléwifi passé en paramétre, au participant passé en parametre
        /// </summary>
        /// <param name="pIdParticipant"></param>
        /// <param name="pHeureArriver"></param>
        /// <param name="pCleWifi"></param>
        internal void EnregistrerArriver(int pIdParticipant, DateTime pHeureArriver,string pCleWifi)
        {
            try
            {
                UneOracleCommand = new OracleCommand("pkg_gestion_ui.maj_heurearriver", CnOracle);
                UneOracleCommand.CommandType = CommandType.StoredProcedure;

                UneOracleCommand.Parameters.Add("p_IdParticipant", OracleDbType.Int64, ParameterDirection.Input).Value = pIdParticipant;
                UneOracleCommand.Parameters.Add("p_HeureArriver", OracleDbType.Date, ParameterDirection.Input).Value = pHeureArriver;
                UneOracleCommand.Parameters.Add("p_CleWifi", OracleDbType.Varchar2, ParameterDirection.Input).Value = pCleWifi;
                UneOracleCommand.ExecuteNonQuery();
                MessageBox.Show("Heure arrivée validée, votre clé wifi est : " + pCleWifi);
            }
            catch (OracleException Oex)
            {
                MessageBox.Show("Erreur Oracle \n" + Oex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Autre Erreur  \n" + ex.Message);
            }
        }


        /// <summary>
        /// Permet d'inscrire un licencie
        /// </summary>
        /// <param name="pNom"></param>
        /// <param name="pPrenom"></param>
        /// <param name="pAdresse1"></param>
        /// <param name="pAdresse2"></param>
        /// <param name="pCp"></param>
        /// <param name="pVille"></param>
        /// <param name="pTel"></param>
        /// <param name="pMail"></param>
        /// <param name="pLicence"></param>
        /// <param name="pQualite"></param>
        /// <param name="pNumCheque"></param>
        /// <param name="pMontantCheque"></param>
        /// <param name="pTypePaiement"></param>
        /// <param name="pLesAteliers"></param>
        public void InscrireLicencie(String pNom, String pPrenom, String pAdresse1, String pAdresse2, String pCp, String pVille, String pTel, String pMail, Int64 pLicence, Int16 pQualite, String pNumCheque, Int16 pMontantCheque, String pTypePaiement, Collection<Int16> pLesAteliers)
        {
            String MessageErreur = "";
            try
            {
                UneOracleCommand = new OracleCommand("pckparticipant.NOUVEAULICENCIE", CnOracle);
                UneOracleCommand.CommandType = CommandType.StoredProcedure;
                UneOracleTransaction = this.CnOracle.BeginTransaction();
                this.ParamCommunsNouveauxParticipants(UneOracleCommand, pNom, pPrenom, pAdresse1, pAdresse2, pCp, pVille, pTel, pMail);
                this.ParamSpecifiquesLicencie(UneOracleCommand, pLicence, pQualite, pNumCheque, pMontantCheque, pTypePaiement, pLesAteliers);


                UneOracleCommand.ExecuteNonQuery();
                UneOracleTransaction.Commit();
            }
            catch (OracleException Oex)
            {
                //MessageErreur="Erreur Oracle \n" + this.GetMessageOracle(Oex.Message);
                MessageBox.Show(Oex.Message);
            }
            catch (Exception ex)
            {

                MessageErreur = "Autre Erreur, les informations n'ont pas été correctement saisies";
            }
            finally
            {
                if (MessageErreur.Length > 0)
                {
                    // annulation de la transaction
                    UneOracleTransaction.Rollback();
                    // Déclenchement de l'exception
                    throw new Exception(MessageErreur);
                }
            }
        }

        /// <summary>
        /// inscrit un licencié, avec nuité
        /// </summary>
        /// <param name="pNom"></param>
        /// <param name="pPrenom"></param>
        /// <param name="pAdresse1"></param>
        /// <param name="pAdresse2"></param>
        /// <param name="pCp"></param>
        /// <param name="pVille"></param>
        /// <param name="pTel"></param>
        /// <param name="pMail"></param>
        /// <param name="pLicence"></param>
        /// <param name="pQualite"></param>
        /// <param name="pNumCheque"></param>
        /// <param name="pMontantCheque"></param>
        /// <param name="pTypePaiement"></param>
        /// <param name="pLesAteliers"></param>
        /// <param name="pLesCategories"></param>
        /// <param name="pLesHotels"></param>
        /// <param name="pLesNuits"></param>
        public void InscrireLicencie(String pNom, String pPrenom, String pAdresse1, String pAdresse2, String pCp, String pVille, String pTel, String pMail, Int64 pLicence, Int16 pQualite, String pNumCheque, Int16 pMontantCheque, String pTypePaiement, Collection<Int16> pLesAteliers,Collection<String> pLesCategories, Collection<String> pLesHotels,Collection<Int16> pLesNuits)
        {
            String MessageErreur = "";
            try
            {
                UneOracleCommand = new OracleCommand("pckparticipant.NOUVEAULICENCIE", CnOracle);
                UneOracleCommand.CommandType = CommandType.StoredProcedure;
                UneOracleTransaction = this.CnOracle.BeginTransaction();
                this.ParamCommunsNouveauxParticipants(UneOracleCommand, pNom, pPrenom, pAdresse1, pAdresse2, pCp, pVille, pTel, pMail);
                this.ParamSpecifiquesLicencie(UneOracleCommand, pLicence, pQualite, pNumCheque, pMontantCheque, pTypePaiement, pLesAteliers);

                //On va créer ici les paramètres spécifiques à l'inscription d'un licencié qui réserve des nuits d'hôtel.
                // Paramètre qui stocke les catégories sélectionnées
                OracleParameter pOraLescategories = new OracleParameter();
                pOraLescategories.ParameterName = "pLesCategories";
                pOraLescategories.OracleDbType = OracleDbType.Char;
                pOraLescategories.CollectionType = OracleCollectionType.PLSQLAssociativeArray;
                pOraLescategories.Value = pLesCategories.ToArray();
                pOraLescategories.Size = pLesCategories.Count;
                UneOracleCommand.Parameters.Add(pOraLescategories);

                // Paramètre qui stocke les hotels sélectionnées
                OracleParameter pOraLesHotels = new OracleParameter();
                pOraLesHotels.ParameterName = "pLesHotels";
                pOraLesHotels.OracleDbType = OracleDbType.Char;
                pOraLesHotels.CollectionType = OracleCollectionType.PLSQLAssociativeArray;
                pOraLesHotels.Value = pLesHotels.ToArray();
                pOraLesHotels.Size = pLesHotels.Count;
                UneOracleCommand.Parameters.Add(pOraLesHotels);

                // Paramètres qui stocke les nuits sélectionnées
                OracleParameter pOraLesNuits = new OracleParameter();
                pOraLesNuits.ParameterName = "pLesNuits";
                pOraLesNuits.OracleDbType = OracleDbType.Int16;
                pOraLesNuits.CollectionType = OracleCollectionType.PLSQLAssociativeArray;
                pOraLesNuits.Value = pLesNuits.ToArray();
                pOraLesNuits.Size = pLesNuits.Count;
                UneOracleCommand.Parameters.Add(pOraLesNuits);


                UneOracleCommand.ExecuteNonQuery();
                UneOracleTransaction.Commit();
            }
            catch (OracleException Oex)
            {
                //MessageErreur="Erreur Oracle \n" + this.GetMessageOracle(Oex.Message);
                MessageBox.Show(Oex.Message);
            }
            catch (Exception ex)
            {

                MessageErreur = "Autre Erreur, les informations n'ont pas été correctement saisies";
            }
            finally
            {
                if (MessageErreur.Length > 0)
                {
                    // annulation de la transaction
                    UneOracleTransaction.Rollback();
                    // Déclenchement de l'exception
                    throw new Exception(MessageErreur);
                }
            }
        }


        /// <summary>
        /// Permet l'envois d'un mail a l'adresse mail passé en parametre depuis l'adresse noreply.maison.des.ligue@gmail.com
        /// </summary>
        /// <param name="pMail"></param>
        public void EnvoisMail(string pMail)
        {
            //Code
            MailMessage mail = new MailMessage();
            mail.From = new System.Net.Mail.MailAddress("noreply.maison.des.ligue@gmail.com");
            SmtpClient smtp = new SmtpClient();
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(mail.From.Address, "adminMDL2016");
            smtp.Host = "smtp.gmail.com";

            //recipient
            mail.To.Add(new MailAddress(pMail));

            mail.IsBodyHtml = true;
            string st = "Inscription réussi";

            mail.Body = st;
            smtp.Send(mail);
        }

    }
}
