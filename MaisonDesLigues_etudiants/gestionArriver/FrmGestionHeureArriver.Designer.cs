﻿namespace gestionArriver
{
    partial class FrmGestionHeureArriver
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmGestionHeureArriver));
            this.GrpHeureArriver = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.BtnValiderHeureArriver = new System.Windows.Forms.Button();
            this.DtpDateArriver = new System.Windows.Forms.DateTimePicker();
            this.CmbParticipant = new System.Windows.Forms.ComboBox();
            this.DtpHeureArriver = new System.Windows.Forms.DateTimePicker();
            this.picQrcode = new System.Windows.Forms.PictureBox();
            this.LblWifi = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.GrpHeureArriver.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picQrcode)).BeginInit();
            this.SuspendLayout();
            // 
            // GrpHeureArriver
            // 
            this.GrpHeureArriver.Controls.Add(this.label3);
            this.GrpHeureArriver.Controls.Add(this.BtnValiderHeureArriver);
            this.GrpHeureArriver.Controls.Add(this.DtpDateArriver);
            this.GrpHeureArriver.Controls.Add(this.CmbParticipant);
            this.GrpHeureArriver.Controls.Add(this.DtpHeureArriver);
            this.GrpHeureArriver.Location = new System.Drawing.Point(12, 12);
            this.GrpHeureArriver.Name = "GrpHeureArriver";
            this.GrpHeureArriver.Size = new System.Drawing.Size(381, 100);
            this.GrpHeureArriver.TabIndex = 18;
            this.GrpHeureArriver.TabStop = false;
            this.GrpHeureArriver.Text = "Heure d\'arriver du participant";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(80, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(183, 13);
            this.label3.TabIndex = 24;
            this.label3.Text = "creer ou recreer une clé et un qrcode";
            // 
            // BtnValiderHeureArriver
            // 
            this.BtnValiderHeureArriver.Location = new System.Drawing.Point(269, 69);
            this.BtnValiderHeureArriver.Name = "BtnValiderHeureArriver";
            this.BtnValiderHeureArriver.Size = new System.Drawing.Size(103, 25);
            this.BtnValiderHeureArriver.TabIndex = 23;
            this.BtnValiderHeureArriver.Text = "Valider";
            this.BtnValiderHeureArriver.UseVisualStyleBackColor = true;
            this.BtnValiderHeureArriver.Click += new System.EventHandler(this.BtnValiderHeureArriver_Click);
            // 
            // DtpDateArriver
            // 
            this.DtpDateArriver.CustomFormat = "dd-MM-yyyy ";
            this.DtpDateArriver.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpDateArriver.Location = new System.Drawing.Point(202, 26);
            this.DtpDateArriver.Name = "DtpDateArriver";
            this.DtpDateArriver.Size = new System.Drawing.Size(102, 20);
            this.DtpDateArriver.TabIndex = 14;
            // 
            // CmbParticipant
            // 
            this.CmbParticipant.FormattingEnabled = true;
            this.CmbParticipant.Location = new System.Drawing.Point(12, 25);
            this.CmbParticipant.Name = "CmbParticipant";
            this.CmbParticipant.Size = new System.Drawing.Size(184, 21);
            this.CmbParticipant.TabIndex = 16;
            this.CmbParticipant.SelectedIndexChanged += new System.EventHandler(this.CmbParticipant_SelectedIndexChanged);
            // 
            // DtpHeureArriver
            // 
            this.DtpHeureArriver.CustomFormat = "HH:mm";
            this.DtpHeureArriver.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpHeureArriver.Location = new System.Drawing.Point(310, 26);
            this.DtpHeureArriver.Name = "DtpHeureArriver";
            this.DtpHeureArriver.ShowUpDown = true;
            this.DtpHeureArriver.Size = new System.Drawing.Size(62, 20);
            this.DtpHeureArriver.TabIndex = 15;
            // 
            // picQrcode
            // 
            this.picQrcode.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.picQrcode.Location = new System.Drawing.Point(121, 142);
            this.picQrcode.Name = "picQrcode";
            this.picQrcode.Size = new System.Drawing.Size(179, 146);
            this.picQrcode.TabIndex = 19;
            this.picQrcode.TabStop = false;
            // 
            // LblWifi
            // 
            this.LblWifi.AutoSize = true;
            this.LblWifi.Location = new System.Drawing.Point(124, 115);
            this.LblWifi.Name = "LblWifi";
            this.LblWifi.Size = new System.Drawing.Size(0, 13);
            this.LblWifi.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 142);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "Votre QrCode :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "Votre code wifi :";
            // 
            // FrmGestionHeureArriver
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(401, 289);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.LblWifi);
            this.Controls.Add(this.picQrcode);
            this.Controls.Add(this.GrpHeureArriver);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmGestionHeureArriver";
            this.Text = "Gestion Heure Arriver";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmGestionHeureArriver_FormClosing);
            this.Load += new System.EventHandler(this.FrmGestionHeureArriver_Load);
            this.GrpHeureArriver.ResumeLayout(false);
            this.GrpHeureArriver.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picQrcode)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox GrpHeureArriver;
        private System.Windows.Forms.Button BtnValiderHeureArriver;
        private System.Windows.Forms.DateTimePicker DtpDateArriver;
        private System.Windows.Forms.ComboBox CmbParticipant;
        private System.Windows.Forms.DateTimePicker DtpHeureArriver;
        private System.Windows.Forms.PictureBox picQrcode;
        private System.Windows.Forms.Label LblWifi;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}

