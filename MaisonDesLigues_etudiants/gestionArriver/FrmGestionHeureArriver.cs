﻿using BaseDeDonnees;
using MaisonDesLigues;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QRCoder;

namespace gestionArriver
{
    public partial class FrmGestionHeureArriver : Form
    {
        /// <summary>
        /// constructeur du formulaire
        /// </summary>
        public FrmGestionHeureArriver()
        {
            InitializeComponent();
        }
        private Bdd UneConnexion;
        private String TitreApplication;
        private String IdStatutSelectionne = "";
        /// <summary>
        /// création et ouverture d'une connexion vers la base de données sur le chargement du formulaire
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmGestionHeureArriver_Load(object sender, EventArgs e)
        {
            UneConnexion = ((FrmLoginHeure)Owner).UneConnexion;
            TitreApplication = ((FrmLoginHeure)Owner).TitreApplication;
            this.Text = TitreApplication;

            //arriver
            UtilitaireHeure.RemplirComboBox(UneConnexion, CmbParticipant, "VPARTICIPANT01");
        }


        /// <summary>
        /// appelle enregistrerArriver, envois l'id participant, la date arriver et une clé wifi
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnValiderHeureArriver_Click(object sender, EventArgs e)
        {
            string cleWifi = UtilitaireHeure.CreerCleWifi();
            UneConnexion.EnregistrerArriver(Convert.ToInt16(CmbParticipant.SelectedValue), System.Convert.ToDateTime(DtpDateArriver.Text + DtpHeureArriver.Text), cleWifi);
            creerQrcode(Convert.ToInt16(CmbParticipant.SelectedValue));
            LblWifi.Text = cleWifi;
        }

        /// <summary>
        /// genere un qrcode avec l'id du participant et l'affiche dans la picturbox.
        /// </summary>
        /// <param name="pIdParticipant"></param>
        public void creerQrcode(int pIdParticipant)
        {
            //Génération  QR Code
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(pIdParticipant.ToString(), QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            //Affichage dans la pic box
            picQrcode.BackgroundImage = qrCode.GetGraphic(5);
        }

        /// <summary>
        /// quand on change de participant, la clé wifi et le qrcode disparaisse
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmbParticipant_SelectedIndexChanged(object sender, EventArgs e)
        {
            LblWifi.Text = "";
            picQrcode.BackgroundImage = null;
        }

        private void FrmGestionHeureArriver_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
