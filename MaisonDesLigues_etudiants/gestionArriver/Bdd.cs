﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using System.Configuration;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;  // bibliothèque pour les expressions régulières
using MaisonDesLigues;
using System.Net.Mail;
using System.Net;
using gestionArriver;

namespace BaseDeDonnees
{
    class Bdd
    {
        //
        // propriétés membres
        //
        private OracleConnection CnOracle;
        private OracleCommand UneOracleCommand;
        private OracleDataAdapter UnOracleDataAdapter;
        private DataTable UneDataTable;
        private OracleTransaction UneOracleTransaction;
        //
        // méthodes
        //
        /// <summary>
        /// constructeur de la connexion 
        /// </summary>
        /// <param name="UnLogin">login utilisateur</param>
        /// <param name="UnPwd">mot de passe utilisateur</param>
        public Bdd(String UnLogin, String UnPwd)
        {
            try
            {
                /// <remarks>on commence par récupérer dans CnString les informations contenues dans le fichier app.config
                /// pour la connectionString de nom StrConnMdl
                /// </remarks>
                ConnectionStringSettings CnString = ConfigurationManager.ConnectionStrings["StrConnMdl"];
                ///<remarks>
                /// on va remplacer dans la chaine de connexion les paramètres par le login et le pwd saisis
                ///dans les zones de texte. Pour ça on va utiliser la méthode Format de la classe String.                /// 
                /// </remarks>
                CnOracle = new OracleConnection(string.Format(CnString.ConnectionString, UnLogin, UnPwd));
                CnOracle.Open();
            }
            catch (OracleException)
            {
                try
                {
                    ConnectionStringSettings CnString = ConfigurationManager.ConnectionStrings["StrConnHome"];
                    CnOracle = new OracleConnection(string.Format(CnString.ConnectionString, UnLogin, UnPwd));
                    CnOracle.Open();

                }
                catch (OracleException Oex)
                {
                    throw new Exception("Erreur à la connexion" + Oex.Message);

                }
            }
        }         
                
            
        
        
        /// <summary>
        /// Méthode permettant de fermer la connexion
        /// </summary>
        public void FermerConnexion()
        {
            this.CnOracle.Close();
        }
        /// <summary>
        /// méthode permettant de renvoyer un message d'erreur provenant de la bd
        /// après l'avoir formatté. On ne renvoie que le message, sans code erreur
        /// </summary>
        /// <param name="unMessage">message à formater</param>
        /// <returns>message formaté à afficher dans l'application</returns>
        private String GetMessageOracle(String unMessage)
        {
            String[] message = Regex.Split(unMessage, "ORA-");
            return (Regex.Split(message[1], ":"))[1];
        }
        /// <summary>
        /// permet de récupérer le contenu d'une table ou d'une vue. 
        /// </summary>
        /// <param name="UneTableOuVue"> nom de la table ou la vue dont on veut récupérer le contenu</param>
        /// <returns>un objet de type datatable contenant les données récupérées</returns>
        public DataTable ObtenirDonnesOracle(String UneTableOuVue)
        {
            string Sql = "select * from " + UneTableOuVue;
            this.UneOracleCommand = new OracleCommand(Sql, CnOracle);
            UnOracleDataAdapter = new OracleDataAdapter();
            UnOracleDataAdapter.SelectCommand = this.UneOracleCommand;
            UneDataTable = new DataTable();
            UnOracleDataAdapter.Fill(UneDataTable);
            return UneDataTable;
        }

        public DataTable ObtenirDonnesOracle(String UneTableOuVue, int idatelier)
        {
            string Sql = "select * from " + UneTableOuVue + " where id =" + idatelier ;
            this.UneOracleCommand = new OracleCommand(Sql, CnOracle);
            UnOracleDataAdapter = new OracleDataAdapter();
            UnOracleDataAdapter.SelectCommand = this.UneOracleCommand;
            UneDataTable = new DataTable();
            UnOracleDataAdapter.Fill(UneDataTable);
            return UneDataTable;
        }

        /// <summary>
        /// Ajoute l'heure d'arriver et la cléwifi passé en paramétre, au participant passé en parametre
        /// </summary>
        /// <param name="pIdParticipant"></param>
        /// <param name="pHeureArriver"></param>
        /// <param name="pCleWifi"></param>
        internal void EnregistrerArriver(int pIdParticipant, DateTime pHeureArriver,string pCleWifi)
        {
            try
            {
                UneOracleCommand = new OracleCommand("pkg_gestion_ui.maj_heurearriver", CnOracle);
                UneOracleCommand.CommandType = CommandType.StoredProcedure;

                UneOracleCommand.Parameters.Add("p_IdParticipant", OracleDbType.Int64, ParameterDirection.Input).Value = pIdParticipant;
                UneOracleCommand.Parameters.Add("p_HeureArriver", OracleDbType.Date, ParameterDirection.Input).Value = pHeureArriver;
                UneOracleCommand.Parameters.Add("p_CleWifi", OracleDbType.Varchar2, ParameterDirection.Input).Value = pCleWifi;
                UneOracleCommand.ExecuteNonQuery();
                MessageBox.Show("Heure arrivée validée");
            }
            catch (OracleException Oex)
            {
                MessageBox.Show("Erreur Oracle \n" + Oex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Autre Erreur  \n" + ex.Message);
            }
        }

       
        /// <summary>
        /// Permet l'envois d'un mail a l'adresse mail passé en parametre depuis l'adresse noreply.maison.des.ligue@gmail.com
        /// </summary>
        /// <param name="pMail"></param>
        public void EnvoisMail(string pMail)
        {
            //Code
            MailMessage mail = new MailMessage();
            mail.From = new System.Net.Mail.MailAddress("noreply.maison.des.ligue@gmail.com");
            SmtpClient smtp = new SmtpClient();
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(mail.From.Address, "adminMDL2016");
            smtp.Host = "smtp.gmail.com";

            //recipient
            mail.To.Add(new MailAddress(pMail));

            mail.IsBodyHtml = true;
            string st = "Inscription réussi";

            mail.Body = st;
            smtp.Send(mail);
        }

    }
}
